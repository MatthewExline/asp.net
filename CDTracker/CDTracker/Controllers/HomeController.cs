﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CDTracker.Models;

namespace CDTracker.Controllers {
    public class HomeController : Controller {
        public IActionResult Index() {
            string pageTitle = "My CD Portfolio";
            ViewBag.pageTitle = pageTitle;
            return View();
        }

        [HttpGet]
        public ViewResult AddCD(){
            return View();
        }

        [HttpPost]
        public ViewResult AddCD(CD cd){
            if(ModelState.IsValid){
                Repository.AddCD(cd);
                return View("Index", Repository.CDs);
            }
            return View("AddCD", cd);
        }
    }    
}