﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace CDTracker.Models {
    public class CD {
        [Required(ErrorMessage = "Please enter the name of the bank.")]
        public string bank { get; set; }

        //[Required(ErrorMessage = "Please enter the term duration.")]
        [Required(AllowEmptyStrings=false, ErrorMessage ="Please enter the term duration.")]
        [Range(1, 60,ErrorMessage ="Please enter the term...")]
        public int? term { get; set; }
        
        [Required(ErrorMessage = "Please enter the rate."),
        RegularExpression(@"\A\d{0,1}\u002E{1}\d{1,3}%{0,1}$",ErrorMessage="Please enter a valid rate*.")]
        //Range(0.01, 9.999, ErrorMessage ="Please enter a valid rate.")]
        public decimal? rate { get{return _rate;} 
            set{
               if(value.ToString().EndsWith("%")){
                  _rate = Decimal.Parse(value.Value.ToString().Substring(0, value.Value.ToString().Length -1)); 
               } else _rate = value;
               //Console.WriteLine("Received: "+value);
               //Console.WriteLine("Given: "+ _rate);
               System.Diagnostics.Debug.WriteLine("Received: "+value);
               System.Diagnostics.Debug.WriteLine("Given: "+ _rate);
            }
        }

        private decimal? _rate;
        
        [Required(ErrorMessage = "Please enter the date of purchase.")]
        public DateTime? purchaseDate { get; set; }
        
        //not required. Must be calculated.
        public DateTime? maturityDate { get; set; }
        
        [Required(ErrorMessage = "Please enter the deposit amount.")]
        public decimal? depositAmount { get; set; }
        
        //not required. Must be calculated.
        public decimal? maturityValue { get; set; }        
        
        public DateTime? calculateMaturityDate(int? term, DateTime? pD){
            return pD.Value.AddMonths(term.Value);
        }
        public decimal? calculateMaturityValue(int? t, decimal? r, decimal? dA){
            decimal? newR = r / 1200;
            for(int i = 0; i < t; i++){
                dA *= (1 + newR);
            } return dA;
        }
    }
}
