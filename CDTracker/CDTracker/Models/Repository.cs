﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDTracker.Models {
    public static class Repository {

        private static List<CD> cds = new List<CD>();
    
        public static IEnumerable<CD> CDs {
            get {return cds; }
        }

        public static void AddCD(CD cd){
            cds.Add(cd);
        }
    }
}

