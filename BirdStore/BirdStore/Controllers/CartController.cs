﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BirdStore.Models;
using BirdStore.Infrastructure;
using BirdStore.Models.ViewModels;

namespace BirdStore.Controllers {
    public class CartController : Controller {
        private IBirdRepository birdRepo;
        private Cart cart;

        public CartController(IBirdRepository birdRepo, Cart cart) {
            this.birdRepo = birdRepo;
            this.cart = cart;
        }

        public ViewResult Index(string returnUrl) {
            return View(new CartIndexViewModel() {
                Cart = cart,
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        public RedirectToActionResult AddToCart( int BirdId, string returnURL) {
            Bird prod = birdRepo.Birds.FirstOrDefault(p => p.BirdId == BirdId);            
            if(null != prod){
                cart.AddItem(prod, 1);
            }
            return RedirectToAction("Index", new { returnURL });
        }
        
        public RedirectToActionResult RemoveFromCart(int BirdId, string returnURL)  {
            Bird prod = birdRepo.Birds.FirstOrDefault(p => p.BirdId == BirdId);
            if (null != prod)  {
                cart.RemoveLine(prod);
            }
            return RedirectToAction("Index", new { returnURL });
        }
    }
}