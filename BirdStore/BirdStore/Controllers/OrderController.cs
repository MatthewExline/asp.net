﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BirdStore.Models;

namespace BirdStore.Controllers {
    public class OrderController : Controller {
        /*public IActionResult Index() {
            return View();
        }
        */
        private IOrderRepository orderRepo;
        private Cart cart;

        public OrderController(IOrderRepository orderRepo, Cart cart) {
            this.orderRepo = orderRepo;
            this.cart = cart;
        }

        public ViewResult Index() {
            return View(orderRepo.Orders.Where(o => !o.Shipped));
        }
        public IActionResult MarkShipped(int orderId) {
            Order order = orderRepo.Orders.FirstOrDefault(o=> o.OrderId == orderId);
            if(order!=null){ 
                order.Shipped = true;
                orderRepo.SaveOrder(order);
            }
            //return RedirectToAction(nameof(Index));//same
            return RedirectToAction("Index");
        }

        public ViewResult Checkout() => View(new Order());

        [HttpPost]
        public IActionResult Checkout(Order order) {
            if(cart.Lines.Count() <= 0) {
                ModelState.AddModelError("EmptyCartErrorInOrderController", "Sorry, your cart is empty!");
            }
            if(ModelState.IsValid) {
                order.Lines = cart.Lines.ToArray();
                orderRepo.SaveOrder(order);
                return RedirectToAction(nameof(Completed));
                //return RedirectToAction("Completed");//this is the same as the line above.
            } else {
                return View(order);
            }
        }
        public ViewResult Completed() {
            cart.Clear();
            return View();//by default, returns a view named "Completed". Will error out if
            //no such view is found.
        }
    }
}