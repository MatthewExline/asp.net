﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BirdStore.Models;
using BirdStore.Models.ViewModels;

namespace BirdStore.Controllers { 
    public class AdminController : Controller {
        private IBirdRepository birdRepo;
        private IAbilityRepository abilityRepo;
        public AdminController(IBirdRepository pRepo, IAbilityRepository abilityRepository) {
            this.birdRepo = pRepo; abilityRepo = abilityRepository;
        }
        public IActionResult Index() {
            AdminViewModel model = new AdminViewModel();
            model.Birds = birdRepo.Birds.ToArray();
            model.Abilities = abilityRepo.Abilities.ToArray();
            return View(model);
        }

        [HttpGet]
        public ViewResult EditBird(int BirdId = 0) {
            //Bird[] birdArray = new Bird[1];
            //if(BirdId==0)
            //    birdArray[0] = new Bird();
            //else
            //    birdArray[0] = birdRepo.Birds.FirstOrDefault(p=>p.BirdId==BirdId);
            //AdminViewModel model = new AdminViewModel{ Birds = birdArray, Abilities = abilityRepo.Abilities.ToArray()};
            //return View(model);
            return View(birdRepo.Birds.FirstOrDefault(p=>p.BirdId==BirdId));
        }
        [HttpGet]
        public ViewResult EditAbility(int AbilityId = 0) {
            if(AbilityId == 0) {
                return View(new Ability());
            }else{            
                return View(abilityRepo.Abilities.FirstOrDefault(p=>p.AbilityId==AbilityId));
            }
        }

        [HttpPost]
        public IActionResult EditBird(Bird bird) {
            if(ModelState.IsValid) {
                birdRepo.SaveBird(bird);
                TempData["message"] = $"{bird.Name} has been saved.";
                return RedirectToAction("Index");
            }else{
                return View(bird);
            }
        }
        [HttpPost]
        public IActionResult EditAbility(Ability Ability) {
            if(ModelState.IsValid) {
                abilityRepo.SaveAbility(Ability);
                TempData["message"] = $"{Ability.Name} has been saved.";
                return RedirectToAction("Index");
            }else{
                return View(Ability);
            }
        }
        [HttpPost]
        public IActionResult Delete(int BirdId) {
            Bird pr = birdRepo.Birds.FirstOrDefault(p=> p.BirdId == BirdId);
            if(pr!=null){
                TempData["message"] = $"{pr.Name} has been deleted.";
                birdRepo.DeleteBird(pr);
            } else {
                
            }
            return View("Index", birdRepo.Birds);
        }
    }
}