﻿using Microsoft.AspNetCore.Mvc;
using BirdStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Components
{
    public class NavigationMenuViewComponent : ViewComponent //this class seems like we made it for dealing with the selection of prod Categories. Not needed if no categories.
    {

        private IBirdRepository birdRepo; //this is the bucket to hold the param from constructor below.

        public NavigationMenuViewComponent(IBirdRepository birdRepo)
        {
            this.birdRepo = birdRepo;
        }

        //this gets invoked with the call to the async method in the _Layout.cshtml
        public IViewComponentResult Invoke() 
        {
            //the category gets passed in URL when you press one of the category buttons, and then it is getting put in the ViewBag
            ViewBag.SelectedCategory = RouteData?.Values["category"];

            return View(

                birdRepo.Birds
                .Select(p => p.Category)
                .Distinct()
                .OrderBy(p => p)

                );
        }

    }
}
