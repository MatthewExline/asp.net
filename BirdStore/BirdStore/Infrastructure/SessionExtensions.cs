﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Infrastructure
{
    public static class SessionExtensions
    {

        //the ISession part of this is really just allowing us to extend Session so we can see this method when we use dot operator.
        public static void SetJson(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));

        }

        //so the return type of T (some type). So if you plug in Cart for T, it has to return a Cart.
        public static T GetJson<T>(this ISession session, string key)
        {
            string sessionData = session.GetString(key);//session data IS stringified JSON.
            return null == sessionData ? default(T) : JsonConvert.DeserializeObject<T>(sessionData);
        }



    }
}
