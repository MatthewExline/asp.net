﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models {
    public interface IBirdAbilityRepository  {
        IEnumerable<Bird>Birds{get;}
        IEnumerable<Ability>Abilities{get;}
        IEnumerable<BirdAbility>BirdAbilities{get;}
    }
}
