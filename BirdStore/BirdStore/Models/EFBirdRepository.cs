﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models
{
    public class EFBirdRepository : IBirdRepository {
        private ApplicationDbContext context;

        public EFBirdRepository(ApplicationDbContext context) {
            this.context = context;
        }

        public IEnumerable<Bird> Birds => context.Birds;//this is how you get at Birds from DB.

        public void SaveBird(Bird Bird) {
            if(Bird.BirdId == 0) {
                context.Birds.Add(Bird);
            }else{
                Bird efBird = context.Birds.FirstOrDefault(p => p.BirdId == Bird.BirdId);
                if(efBird != null) {
                    efBird.Name = Bird.Name;
                    efBird.Description = Bird.Description;
                    efBird.Price = Bird.Price;
                    efBird.Category = Bird.Category;
                }
            }
            context.SaveChanges();
        }
        public Bird DeleteBird(Bird Bird) {
            Bird pr = context.Birds.FirstOrDefault(p=>p.BirdId == Bird.BirdId);
            if(pr!=null){
                context.Birds.Remove(pr);
            }context.SaveChanges();
            return pr;
        }
    }
}
