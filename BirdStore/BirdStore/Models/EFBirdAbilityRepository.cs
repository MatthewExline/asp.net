﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models {
    public class EFBirdAbilityRepository : IBirdAbilityRepository {
        public ApplicationDbContext context;
        public IEnumerable<Bird>Birds =>context.Birds;
        public IEnumerable<Ability>Abilities=>context.Abilities;
        public IEnumerable<BirdAbility>BirdAbilities=>context.BirdAbilities;

        public EFBirdAbilityRepository(ApplicationDbContext context) {
            this.context = context;
        }
    }
}
