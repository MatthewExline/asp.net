﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models {
    public class Image {
        public int ImageId{get; set;}
        public String Path{get; set; }
        public int BirdId {get; set;}
        public Bird Bird{get; set;}
    }
}
