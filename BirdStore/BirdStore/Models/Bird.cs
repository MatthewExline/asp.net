﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models
{
    public class Bird
    {
        public int BirdId { get; set; }
        [Required(ErrorMessage = "Please enter a name.")]
        public string Name { get; set; }
        [Required(ErrorMessage ="Please enter a description.")]
        public string Description { get; set; }
        [Required]
        [Range(0.01, double.MaxValue, ErrorMessage ="Please enter a positive vale.")]
        public decimal Price { get; set; }
        [Required(ErrorMessage = "Please specify a category.")]
        public string Category { get; set; }
        //[Required(ErrorMessage="Please set the abilities.")]
        public List<BirdAbility>Abilities{get; set;}

        public List<Image>Images{get; set;}
        [NotMapped]
        public List<Ability>Abilities2{get; set;}
        [NotMapped]
        public List<String>Images2{get; set;}
        
    }
}
