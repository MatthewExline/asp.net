﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models
{
    public class EFIMageRepository : IImageRepository {
        private ApplicationDbContext context;

        public EFIMageRepository(ApplicationDbContext context) {
            this.context = context;
        }

        public IEnumerable<Image> Images => context.Images;//this is how you get at Birds from DB.

        public void SaveImage(Image Image) {
            if(Image.ImageId == 0) {
                context.Images.Add(Image);
            }else{
                Image efImage = context.Images.FirstOrDefault(p => p.ImageId == Image.ImageId);
                if(efImage != null) {
                    efImage.Bird = Image.Bird;
                    efImage.BirdId = Image.BirdId;
                    efImage.Path = Image.Path;
                    efImage.BirdId = Image.BirdId;
                }
            }
            context.SaveChanges();
        }
        public Image DeleteImage(Image Image) {
            Image pr = context.Images.FirstOrDefault(p=>p.ImageId == Image.ImageId);
            if(pr!=null){
                context.Images.Remove(pr);
            }context.SaveChanges();
            return pr;
        }
    }
}
