﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models {
    public interface IImageRepository {        
        IEnumerable<Image> Images { get; }
        void SaveImage(Image Image);
        Image DeleteImage(Image Image);
    }
}