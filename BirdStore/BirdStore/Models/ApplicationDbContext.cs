﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models{   
    public class ApplicationDbContext : DbContext {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) {
        
        }

        public DbSet<Bird> Birds { get; set; } 
        public DbSet<Ability>Abilities{get; set;}
        public DbSet<Image> Images{get; set;}
        public DbSet<BirdAbility>BirdAbilities{get; set;}
        public DbSet<Order> Orders {get; set;}

        protected override void OnModelCreating(ModelBuilder mB) {
            mB.Entity<BirdAbility>()
                .HasKey(t => new {t.BirdId, t.AbilityId});
            
            mB.Entity<BirdAbility>()
                .HasOne(p => p.Bird)
                .WithMany(c => c.Abilities)
                .HasForeignKey(p => p.BirdId);

            mB.Entity<BirdAbility>()
                .HasOne(c => c.Ability)
                .WithMany(p => p.Birds)
                .HasForeignKey(c => c.AbilityId);                
        }
    }
}
