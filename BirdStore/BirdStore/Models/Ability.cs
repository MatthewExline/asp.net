﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models {
    public class Ability {
        public int AbilityId{get; set; }
        [Required(ErrorMessage = "Please enter a name.")]
        public string Name{get; set;}
        public List<BirdAbility>Birds{get; set;}
    }
}
