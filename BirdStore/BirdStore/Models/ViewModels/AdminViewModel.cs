﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models.ViewModels
{
    public class AdminViewModel{
        public Bird[]Birds{get;set;}
        public Ability[]Abilities{get;set;}
    }
}
