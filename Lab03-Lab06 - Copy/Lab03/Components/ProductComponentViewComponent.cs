﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Lab03.Models;
using Lab03.Models.ViewModels;

namespace Lab03.Components {
    public class ProductComponentViewComponent : ViewComponent {
        private iProductRepo prodRepo;
        private iCityRepo cityRepo;
        private iCityProductRepo cityProdRepo;
        private Dictionary<int, string> cityList;
        private Dictionary<int, string> productList;
        private Dictionary<int, string>[] model;
        public ProductComponentViewComponent(iProductRepo p, iCityRepo c, iCityProductRepo cp ) {
            prodRepo = p;    cityRepo = c;    cityProdRepo = cp;
        }
        public IViewComponentResult Invoke(Negotiation negotiation) {
        //public async Task<IViewComponentResult> InvokeAsync(){
            CityProduct[] cps = cityProdRepo.CityProducts.ToArray();
            City[] c = cityRepo.Cities.ToArray();
            Product[] p = prodRepo.Products.ToArray();
            
            cityList = new Dictionary<int, string>();
            for(int i = 0; i < c.Length; i++) {
                cityList.Add(c[i].CityId, c[i].Name);
            }
            productList = new Dictionary<int, string>();
            for(int i = 0; i < p.Length; i++) {
                productList.Add(p[i].productId, p[i].productName);
            }
            model = new Dictionary<int, string>[2];
            model[0] = cityList;
            model[1] = productList;
            if(negotiation != null && negotiation.exporterHasBeenSet) {
                return View("SetProduct", model);
            }else if(negotiation != null) {
                return View("SetExporter", model);
            } else {
                return View("Default", model);//the default view is the set-importer.
            }
        }
    }
}
