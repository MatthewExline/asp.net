﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Lab03.Migrations
{
    public partial class AddingTradeAgreements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_CityProducts",
                table: "CityProducts");

            migrationBuilder.AddColumn<int>(
                name: "CurrentSurplus",
                table: "CityProducts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MaximumPotentialUnitProduction",
                table: "CityProducts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "PerUnitMarketValue",
                table: "CityProducts",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PerUnitProductionPrice",
                table: "CityProducts",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ScheduledAnnualUnitProduction",
                table: "CityProducts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_CityProducts",
                table: "CityProducts",
                columns: new[] { "CityId", "ProductId" });

            migrationBuilder.CreateTable(
                name: "TradeAgreements",
                columns: table => new
                {
                    ImporterId = table.Column<int>(nullable: false),
                    ExporterId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    UnitPrice = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TradeAgreements", x => new { x.ImporterId, x.ExporterId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_TradeAgreements_Cities_ExporterId",
                        column: x => x.ExporterId,
                        principalTable: "Cities",
                        principalColumn: "CityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TradeAgreements_Cities_ImporterId",
                        column: x => x.ImporterId,
                        principalTable: "Cities",
                        principalColumn: "CityId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_TradeAgreements_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "productId",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateIndex(
                name: "IX_TradeAgreements_ImporterId",
                table: "TradeAgreements",
                column: "ImporterId");

            migrationBuilder.CreateIndex(
                name: "IX_TradeAgreements_ExporterId",
                table: "TradeAgreements",
                column: "ExporterId");

            migrationBuilder.CreateIndex(
                name: "IX_TradeAgreements_ProductId",
                table: "TradeAgreements",
                column: "ProductId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TradeAgreements");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CityProducts",
                table: "CityProducts");

            migrationBuilder.DropColumn(
                name: "CurrentSurplus",
                table: "CityProducts");

            migrationBuilder.DropColumn(
                name: "MaximumPotentialUnitProduction",
                table: "CityProducts");

            migrationBuilder.DropColumn(
                name: "PerUnitMarketValue",
                table: "CityProducts");

            migrationBuilder.DropColumn(
                name: "PerUnitProductionPrice",
                table: "CityProducts");

            migrationBuilder.DropColumn(
                name: "ScheduledAnnualUnitProduction",
                table: "CityProducts");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CityProducts",
                table: "CityProducts",
                columns: new[] { "CityId", "ProductId", "Type" });
        }
    }
}
