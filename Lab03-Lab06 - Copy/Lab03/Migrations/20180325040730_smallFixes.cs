﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Lab03.Migrations
{
    public partial class smallFixes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PerUnitMarketValue",
                table: "CityProducts",
                newName: "PerUnitLocalValue");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PerUnitLocalValue",
                table: "CityProducts",
                newName: "PerUnitMarketValue");
        }
    }
}
