﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Lab03.Models;
using Lab03.Models.ViewModels;

namespace Lab03.Controllers {
    public class CityController : Controller {
        private iCityRepo cityRepo;
        private iContinentRepo contRepo;
        private iCityProductRepo cityProductRepo;
        private iProductRepo productRepo;
        private int PageSize = 3;
        public CityController(iCityRepo cityr, iContinentRepo contr, iCityProductRepo cpr, iProductRepo pr) {
            cityRepo = cityr; contRepo = contr; cityProductRepo = cpr; productRepo = pr;
        }
        [Route("/City/CityDetails", Name = "viewCity")]
        public ViewResult ViewCity(int CityId, int importsPageNumber = 1, int exportsPageNumber = 1, int producesPageNumber = 1) {
            City city = cityRepo.Cities.FirstOrDefault(p => p.CityId == CityId);            
            CityProduct[] cityProducts = cityProductRepo.CityProducts.ToArray();
            Product[] products = productRepo.Products.ToArray();
            List<City.ProductInfo> simpleImports = new List<City.ProductInfo>();
            List<City.ProductInfo> simpleExports = new List<City.ProductInfo>();
            List<City.ProductInfo> simpleProduce = new List<City.ProductInfo>();
            CityProduct[] productsOfThisCity = new CityProduct[productRepo.Products.Count()];
            int i = 0;
            for(int j = 0; j < cityProducts.Length; j++) {
                if(cityProducts[j].CityId == city.CityId) {
                    productsOfThisCity[i] = cityProducts[i++];
                    for(int k = 0; k < products.Length; k++) {
                        if(cityProducts[j].ProductId == products[k].productId) {
                            if(cityProducts[j].Type == 2 || cityProducts[j].Type == 4 ||
                                cityProducts[j].Type == 5 || cityProducts[j].Type == 6) {
                                City.ProductInfo pI = new City.ProductInfo(
                                    cityProducts[j].ProductId, products[k].productName, cityProducts[j].PerUnitLocalValue, cityProducts[j].PerUnitProductionPrice,
                                    cityProducts[j].ScheduledAnnualUnitProduction, cityProducts[j].MaximumPotentialUnitProduction
                                );
                                simpleImports.Add(pI);
                            } 
                            if(cityProducts[j].Type == 3 || cityProducts[j].Type == 4 ||
                                cityProducts[j].Type == 6 || cityProducts[j].Type == 7) {
                                City.ProductInfo pI = new City.ProductInfo(
                                    cityProducts[j].ProductId, products[k].productName, cityProducts[j].PerUnitLocalValue, cityProducts[j].PerUnitProductionPrice,
                                    cityProducts[j].ScheduledAnnualUnitProduction, cityProducts[j].MaximumPotentialUnitProduction
                                );
                                simpleExports.Add(pI);
                            }
                            if(cityProducts[j].Type == 1 || cityProducts[j].Type == 2 ||
                                cityProducts[j].Type == 3 || cityProducts[j].Type == 4) {
                                City.ProductInfo pI = new City.ProductInfo(
                                    cityProducts[j].ProductId, products[k].productName, cityProducts[j].PerUnitLocalValue, cityProducts[j].PerUnitProductionPrice,
                                    cityProducts[j].ScheduledAnnualUnitProduction, cityProducts[j].MaximumPotentialUnitProduction
                                );
                                simpleProduce.Add(pI);
                            }
                            break;
                        }
                    }
                }
            }
            city.ContinentName = contRepo.Continents.First(c => c.ContinentId == city.ContinentId).Name;
            city.ProductsImported2 = simpleImports.ToArray();
            city.ProductsExported2 = simpleExports.ToArray();
            city.ProductsProduced2 = simpleProduce.ToArray();
            PagingInfo importsPages = new PagingInfo() {
                CurrentPage = importsPageNumber,
                ItemsPerPage = PageSize,
                TotalItems = city.ProductsImported2.Length
            };
            PagingInfo exportsPages = new PagingInfo() {
                CurrentPage = exportsPageNumber,
                ItemsPerPage = PageSize,
                TotalItems = city.ProductsExported2.Length
            };
            PagingInfo producesPages = new PagingInfo() {
                CurrentPage = producesPageNumber,
                ItemsPerPage = PageSize,
                TotalItems = city.ProductsProduced2.Length
            };
            CityViewModel cvm = new CityViewModel {
                City = city, ImportsPages = importsPages, ExportsPages = exportsPages, ProducesPages=producesPages, CityProducts = productsOfThisCity
            };
            return View("CityDetails", cvm);            
        }
    }
}