﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab03.Models;
using Lab03.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Lab03.Controllers {
    public class ProductTopographyController : Controller {
        private iProductRepo prodRepo;
        private iCityRepo cityRepo;
        private iCityProductRepo cityProdRepo;
        private iContinentRepo continentRepo;
        private iTradeAgreementRepo tradeRepo;
        private CityProductFull model;
        private Negotiation neg;
        public ProductTopographyController(iProductRepo p, iCityRepo c, iCityProductRepo cp, iContinentRepo co, iTradeAgreementRepo t, Negotiation n) {
            prodRepo = p; cityRepo = c; cityProdRepo = cp; continentRepo = co; tradeRepo = t; neg = n;
        }
        public IActionResult Index(int CityId = 0, int ProductId = 0) {
            //if(CityId != 0 && ProductId != 0) {
            //    City city = cityRepo.Cities.FirstOrDefault(p => p.CityId == CityId);
            //    Product product = prodRepo.Products.First(p => p.productId == ProductId);
            //    CityProduct cp = cityProdRepo.CityProducts.FirstOrDefault(c => c.CityId == CityId && c.ProductId == ProductId);
            //    model = new CityProductFull {
            //        CityProduct = cp, Product = product, City = city
            //    };
            //    return View(model);
            //}
            //else return View();
            return View();
        }
        [HttpPost]
        public ViewResult SetImporter(int CityId) {
            City imp = cityRepo.Cities.FirstOrDefault(p => p.CityId == CityId);
            neg.SetImporter(imp);
            neg.importerHasBeenSet = true;
            return View("Index", neg);
        }
        [HttpPost]
        public IActionResult SetExporter(int CityId) {
            if(neg.hasImporterBeenSet()) throw new Exception("GOOD");
            City exp = cityRepo.Cities.FirstOrDefault(p => p.CityId == CityId);
            neg.SetExporter(exp);
            neg.exporterHasBeenSet = true;
            return View("Index", neg);
        }
        [HttpPost]
        public IActionResult SetProduct(int ProductId) {
            Product pr = prodRepo.Products.FirstOrDefault(p => p.productId == ProductId);
            neg.SetProduct(pr, 5, (decimal)6.55);
            TradeAgreement ta = new TradeAgreement {
                ExporterId = neg.Exporter.CityId, Exporter = neg.Exporter,
                ImporterId = neg.Importer.CityId, Importer = neg.Exporter,
                ProductId = neg.Product.productId, Product = neg.Product,
                Quantity = neg.Quantity, UnitPrice = neg.Price
            };
            tradeRepo.Save(ta);
            return View("Index", neg);
        }
    }
}