﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab03.Models;
using Lab03.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Lab03.Controllers{
    public class HomeController : Controller{
        
        private iCityRepo cityRepo;
        private iContinentRepo contRepo;
        private iCityProductRepo cityProductRepo;
        private iProductRepo productRepo;

        public HomeController(iCityRepo cityr, iContinentRepo contr, iCityProductRepo cpr, iProductRepo pr) {
            cityRepo = cityr; contRepo = contr; cityProductRepo = cpr; productRepo = pr;
        }

        public IActionResult Index(){
            double totalGDP = 0;
            double southernGDP=0;
            double northernGDP=0;
            City[] cities = cityRepo.Cities.ToArray();
            foreach(City c in cities){
                totalGDP += c.GDP;
                if(c.Coordinate4 <.64) southernGDP+= c.GDP;
                else northernGDP += c.GDP;
            }
            ViewBag.WorldMarketValue = totalGDP;
            ViewBag.SouthernGDP = southernGDP;
            ViewBag.NorthernGDP = northernGDP;

            IndexViewModel model = new IndexViewModel();

            CityProduct[] cityProducts = cityProductRepo.CityProducts.ToArray();
            Product[] products = productRepo.Products.ToArray();
            Continent[] continents = contRepo.Continents.ToArray();
            for(int i = 0; i < cities.Length; i++) {
                List<string> simpleImports = new List<string>();
                List<string> simpleExports = new List<string>();
                for(int j = 0; j < cityProducts.Length; j++) {
                    if(cityProducts[j].CityId == cities[i].CityId) {
                        for(int k = 0; k < products.Length; k++) {
                            if(cityProducts[j].ProductId == products[k].productId) {
                                if(cityProducts[j].Type == 0) {
                                    simpleImports.Add(products[k].productName);
                                } else {                                    
                                    simpleExports.Add(products[k].productName);
                                }
                                break;
                            }
                        }
                    }
                }
                cities[i].ProductsImported = simpleImports.ToArray();
                cities[i].ProductsExported = simpleExports.ToArray();
                cities[i].ContinentName = contRepo.Continents.First(c => c.ContinentId == cities[i].ContinentId).Name;
            }
            model.Cities = cities;
            return View(model);
        }
    }
}