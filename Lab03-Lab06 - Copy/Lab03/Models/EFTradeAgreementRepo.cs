﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models
{
    public class EFTradeAgreementRepo : iTradeAgreementRepo{
        public DBContext context;
        public IEnumerable<TradeAgreement> TradeAgreements => context.TradeAgreements;

        public EFTradeAgreementRepo(DBContext context) {
            this.context = context;
        }
        public void Save(TradeAgreement t) {
            context.TradeAgreements.Attach(t);
            context.SaveChanges();
        }
    }
}
