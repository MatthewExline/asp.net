﻿using Lab03.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models{
    public class SessionNegotiation : Negotiation {
        [JsonIgnore]
        public ISession Session { get; set; }

        public static Negotiation GetNegotiation(IServiceProvider services) {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?.HttpContext.Session;
            SessionNegotiation n = session?.GetJson<SessionNegotiation>("Negotiation") ?? new SessionNegotiation();
            n.Session = session;
            return n;
        }
        public override void SetProduct(Product p, int qty, decimal d) {
            Session.SetJson("Negotiation", this);
        }
        public override void SetImporter(City p) {
            Session.SetJson("Negotiation", this);
        }
        public override void SetExporter(City c) {
            Session.SetJson("Negotiation", this);
        }
        public override void Clear() {
            base.Clear();
            Session.Remove("Negotiation");
        }
    }
}
