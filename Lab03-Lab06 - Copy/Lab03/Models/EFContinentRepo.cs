﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models {
    public class EFContinentRepo : iContinentRepo {
        public DBContext context;
        public IEnumerable<Continent> Continents => context.Continents;

        public EFContinentRepo(DBContext context) {
            this.context = context;
        }
    }
}