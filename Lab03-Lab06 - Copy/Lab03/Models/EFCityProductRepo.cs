﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models {
    public class EFCityProductRepo : iCityProductRepo {
        public DBContext context;
        public IEnumerable<City>Cities =>context.Cities;
        public IEnumerable<Product>Products=>context.Products;
        public IEnumerable<CityProduct>CityProducts=>context.CityProducts;

        public EFCityProductRepo(DBContext context) {
            this.context = context;
        }
    }
}
