﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models {

    public class CityProduct {
        //public int CityProductExportId { get; set; }
        //Our example has: public int AuthorOrder{get;set;}  but I don't know how that would be implemented here or if it's necessary?

        public City City { get; set; }
        public int CityId { get; set; }

        public Product Product { get; set; }
        public int ProductId { get; set; }
        
        public int Type { get; set; }//0:import or 1:export  
        // N E W   C O D I N G   S Y S T E M    F O R   T Y P E :
        //  0: Does not Import, Export, or Produce
        //  1: Product is ONLY PRODUCED HERE.
        //  2: Product is ONLY PRODUCED AND IMPORTED.
        //  3: PRODUCT IS ONLY PRODUCED AND EXPORTED.
        //  4: PRODUCT IS PRODUCED, IMPORTED, AND EXPORTED.
        //  5: PRODUCT IS ONLY IMPORTED.
        //  6: PRODUCT IS ONLY IMPORTED AND EXPORTED.
        //  7: PRODUCT IS ONLY EXPORTED.//This situation can only exist if City has surplus from an earlier point in time.

        //PerUnitProductionPrice is NULLABLE because the Price to Produce the Product is NULL if the Product cannot be produced here.
        public decimal? PerUnitProductionPrice { get; set; }
        //Setting the ProductionPrice to ZERO in situations where the Product cannot be produced would be wrong, it would indicate 
        //that the Product was FREE to produce.

        public decimal PerUnitLocalValue { get; set; }
        public int CurrentSurplus { get; set; }
        public void FixType() {

        }
        [NotMapped]
        private int _scheduledAnnualProduction;
        //In this case, if the product cannot be produced here, then setting this to ZERO is okay. 
        //MUST CHECK TO SEE THAT TYPE IS : (1, 2, 3, or 4) before setting this to any number other than zero.
        //IF SETTING TO ZERO, SET TYPE TO : ( 0, 5, 6, or 7 ).
        public int ScheduledAnnualUnitProduction { get { return _scheduledAnnualProduction; } set {
            if(value > MaximumPotentialUnitProduction){
                throw new Exception("An attempt was made to set the Scheduled Annual Production of CityId: " +
                   CityId.ToString()+", ProductId: "+ProductId.ToString()+" to a value greater than it's Maximum Potential" +
                   " Production Value.");
            } else if(value >= 0 ) {
                if(value == 0) {
                    if(Type == 1) Type = 0;
                    else if(Type == 2) Type = 5;
                    else if(Type == 3){
                       if (CurrentSurplus > 0)Type = 7;
                       else Type = 0;
                    } else if(Type == 4) Type = 6;
                }else{
                   if(Type == 0) Type = 1;
                   else if(Type == 5) Type = 2;
                   else if(Type == 7) Type = 3;
                   else if(Type == 6) Type = 4;
                }
                _scheduledAnnualProduction = value;
            }else{
               throw new Exception("An attempt was made to set the Scheduled Annual Production of CityId: " +
                   CityId.ToString()+", ProductId: "+ProductId.ToString()+" to a negative value.");
            }
        }}
        [NotMapped]
        private int _maximumPotentialUnitProduction;
        public int MaximumPotentialUnitProduction {
            get { return _maximumPotentialUnitProduction; }
            set {
                if(value < 0) throw new Exception("An attempt was made to set the MaxPotentialUnitProduction of CityId: " +
                   CityId.ToString() + ", ProductId: " + ProductId.ToString() + " to a negative value.");
                else if(value < ScheduledAnnualUnitProduction) {
                    ScheduledAnnualUnitProduction = value;                    
                }
                _maximumPotentialUnitProduction = value;
            }
        }
    }    
}
