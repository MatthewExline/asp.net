﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models{
    public class City{
        public int CityId{get; set;}
        public string Name{get; set;}
        public double GDP{get; set;}
        public long Population{get; set;}
        public double Coordinate1 {get; set;}
        public double Coordinate2 { get; set; }
        public double Coordinate3 { get; set; }
        public double Coordinate4 { get; set; }

        [JsonIgnore]
        public List<CityProduct> Products{get; set;}
        [JsonIgnore]
        public List<TradeAgreement> TradeAgreementAsImporter { get; set; }
        [JsonIgnore]
        public List<TradeAgreement> TradeAgreementAsExporter { get; set; }

        public int ContinentId{get; set;}

        [JsonIgnore]
        public Continent Continent {get; set;}

        [NotMapped]
        public string ContinentName{get; set;}

        [NotMapped]
        public string[] ProductsImported { get; set; }

        [NotMapped]
        public string[] ProductsProduced { get; set; }

        [NotMapped]
        public string[] ProductsExported { get; set; }

        [NotMapped]
        public ProductInfo[] ProductsImported2{get; set;}

        [NotMapped]
        public ProductInfo[] ProductsExported2{get; set;}

        [NotMapped]
        public ProductInfo[] ProductsProduced2 { get; set; }

        [NotMapped]
        public class ProductInfo {
            public ProductInfo(int ProductID, string name,
                decimal PerUnitLocalValue, decimal? PerUnitProductionPrice,
                int ScheduledAnnualUnitProduction, int MaximumPotentialUnitProduction) {
                    this.ProductName = name; this.PerUnitLocalValue = PerUnitLocalValue;
                    this.ProductId = ProductID; this.PerUnitProductionPrice = PerUnitProductionPrice;
                    this.ScheduledAnnualUnitProduction = ScheduledAnnualUnitProduction;
                    this.MaximumPotentialUnitProduction = MaximumPotentialUnitProduction;
            }
            public string ProductName{get; set;}
            public int ProductId{get; set;}
            public decimal PerUnitLocalValue { get; set; }
            public decimal? PerUnitProductionPrice { get; set; }
            public int ScheduledAnnualUnitProduction { get; set; }
            public int MaximumPotentialUnitProduction { get; set; }
        }
    }
}
