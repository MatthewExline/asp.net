﻿namespace Lab03.Models {
    public class Negotiation {
        public City Importer = new City();
        public City Exporter = new City();
        public Product Product = new Product();
        public int Quantity;
        public decimal Price;
        public bool quantityHasBeenSet = false;
        public bool priceHasBeenSet = false;
        public bool productHasBeenSet = false;
        public bool importerHasBeenSet = false;
        public bool exporterHasBeenSet = false;

        public virtual void SetProduct(Product product, int qty, decimal price) {
            this.Product = product;   this.Quantity = qty; this.Price = price;
            productHasBeenSet = true;  quantityHasBeenSet = true; priceHasBeenSet = true;
        }
        public virtual void SetImporter(City importer) {
            this.Importer = importer;
            importerHasBeenSet = true;
        }
        public virtual void SetExporter(City exporter) {
            this.Exporter = exporter;
            exporterHasBeenSet = true;
        }
        public virtual void Clear() {
            quantityHasBeenSet = false;
            priceHasBeenSet = false;
            Importer = null;
            Exporter = null;
            Product = null;
            importerHasBeenSet = false;
            exporterHasBeenSet = false;
            productHasBeenSet = false;
        }
        public bool hasPriceBeenSet() {
            return priceHasBeenSet;
        }
        public bool hasQuantityBeenSet() {
            return quantityHasBeenSet;
        }
        public bool hasImporterBeenSet() {
            return importerHasBeenSet;
        }
        public bool hasExporterBeenSet() {
            return exporterHasBeenSet;
        }
        public bool hasProductBeenSet() {
            return productHasBeenSet;
        }
    }
}