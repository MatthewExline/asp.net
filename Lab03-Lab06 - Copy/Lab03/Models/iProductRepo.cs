﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models {
    public interface iProductRepo{
        IEnumerable<Product> Products { get; }
    }
}
