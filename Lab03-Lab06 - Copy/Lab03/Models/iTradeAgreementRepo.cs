﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models
{
    public interface iTradeAgreementRepo {
        IEnumerable<TradeAgreement> TradeAgreements { get; }
        void Save(TradeAgreement t);
    }
}
