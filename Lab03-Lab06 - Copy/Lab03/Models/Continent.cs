﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models {
    public class Continent {
        public int ContinentId{get; set;}
        public String Name{get; set;}

        public List<City>Cities{get; set;}
    }
}
