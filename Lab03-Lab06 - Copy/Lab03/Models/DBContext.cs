﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models  {
    public class DBContext : DbContext {
        public DBContext(
            DbContextOptions<DBContext>options) : base(options) {}
        
        public DbSet<City> Cities{get; set;}
        public DbSet<Continent> Continents { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<CityProduct> CityProducts { get; set; }
        public DbSet<TradeAgreement> TradeAgreements { get; set; }
        
        
        protected override void OnModelCreating(ModelBuilder mB) {
            mB.Entity<CityProduct>()
                .HasKey(t => new { t.CityId, t.ProductId });//, t.Type});
            
            mB.Entity<CityProduct>()
                .HasOne(p => p.Product)
                .WithMany(c => c.Cities)
                .HasForeignKey(p => p.ProductId);

            mB.Entity<CityProduct>()
                .HasOne(c => c.City)
                .WithMany(p => p.Products)
                .HasForeignKey(c => c.CityId);
            
            mB.Entity<TradeAgreement>()
                .HasKey(t => new { t.ImporterId, t.ExporterId, t.ProductId });

            mB.Entity<TradeAgreement>()
                .HasOne(p => p.Product)//Keep in mind that Exporter and Importer are not cities, they are CityProducts.
                .WithMany(c => c.Market)
                .HasForeignKey(p => p.ProductId);

            mB.Entity<TradeAgreement>()
                .HasOne(p => p.Exporter)//Keep in mind that Exporter and Importer are not cities, they are CityProducts.
                .WithMany(c => c.TradeAgreementAsExporter)
                .HasForeignKey(p => p.ExporterId);

            mB.Entity<TradeAgreement>()
                .HasOne(c => c.Importer)
                .WithMany(p => p.TradeAgreementAsImporter)
                .HasForeignKey(c => c.ImporterId);
                
        }
        
    }
}
