﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models.ViewModels {
    public class CityViewModel {
        public City City { get; set; }
        public PagingInfo ImportsPages { get; set; }
        public PagingInfo ExportsPages { get; set; }
        public PagingInfo ProducesPages{ get; set; }
        public CityProduct[] CityProducts { get; set; }
    }
}
