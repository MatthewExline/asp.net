﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models.ViewModels
{
    public class CityProductFull {
        public CityProduct CityProduct { get; set; }
        public City City { get; set; }
        public Product Product { get; set; }
    }
}
