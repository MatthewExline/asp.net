﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models {
    public class TradeAgreement {

        public City Importer { get; set; }
        public int ImporterId { get; set; }

        public City Exporter { get; set; }
        public int ExporterId { get; set; }

        public Product Product {get; set;}
        public int ProductId { get; set; }

        public int Quantity { get; set; }

        public decimal UnitPrice { get; set; }
    }
}
