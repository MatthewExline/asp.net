﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models {
    public class Product {
        public int productId{get; set;}
        public String productName{get; set;}
        public List<CityProduct>Cities{get; set;}
        public List<TradeAgreement> Market { get; set; }

        public string Category {get; set;}//Product, service, resource, capital
        //for the purposes of this project, products are consumer-ready things.
        // services is self-explanitory
        // Resources are RAW RESOURCES such as crude petroleum, nickel, iron, gold, etc.
        // Capital is INDUSTRIAL GRADE PRODUCTS such as factory parts.
    }
}
