﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace Lab03.Models {
    public static class MySeeds {
        public static void Populate(IApplicationBuilder app) {
            
            DBContext context = app.ApplicationServices.GetRequiredService<DBContext>();
            List<Continent> continents = context.Continents.ToList();
            //context.ChangeTracker.QueryTrackingBehavior = Microsoft.EntityFrameworkCore.QueryTrackingBehavior.NoTracking;
            List<Product> products = context.Products.ToList();
            List<City> cities = context.Cities.ToList();
            /*
                        context.Continents.AddRange(
                                new Continent {
                                    Name = "North America"
                                }, new Continent {
                                    Name = "South America"
                                }, new Continent {
                                    Name = "Asia"
                                }, new Continent {
                                    Name = "Africa"
                                }, new Continent {
                                    Name = "Europe"
                                }, new Continent {
                                    Name = "Pacific"
                                }
                            );
                        context.SaveChanges();
        
                        context.Cities.AddRange(
                            new City {
                                Name = "Russia", Population = 144300000, Continent = continents[2],
                                GDP = 1283, Coordinate1 = .581, Coordinate2 = .638, Coordinate3 = .818,
                                Coordinate4 = .738
                            },
                            new City{Name="North Dakota",
                                Population=757952,
                                GDP=27.72, Continent = continents[0], Coordinate1 = .169,
                                Coordinate2 = .186, Coordinate3 = .803, Coordinate4 = .782
                            },
                            new City{Name="La Crosse", Continent = continents[0],
                                Population=52109, GDP= 6.321, Coordinate1 = .240,
                                Coordinate2 = .286, Coordinate3 = .170, Coordinate4 = .111
                            },
                            new City{Name="West Coast", Population=50630000,//50.6 Million
                                GDP= 3100,Continent = continents[0], Coordinate1 = .662,
                                Coordinate2 = .814, Coordinate3 = .340, Coordinate4 = .202
                            },
                            new City{Name="East Coast", Population=112600000,//112 Million
                                GDP = 4500, Continent = continents[0], Coordinate1 = .702,
                                Coordinate2 = .754, Coordinate3 = .463, Coordinate4 = .400
                            },
                            new City{Name="Cuba", Population= 11480000,//11.48 Million
                                GDP = 87.13, Continent = continents[0], Coordinate1 = .754,
                                Coordinate2 = .808, Coordinate3 = .541, Coordinate4 = .474
                            },
                            new City{Name="Mexico", Population=127500000, //127.5 Million
                                GDP = 1046.92,//gdp is in billions.
                                Continent = continents[1], Coordinate1 = .776, Coordinate2 = .886,
                                Coordinate3 = .923, Coordinate4 = .755
                            },                
                            new City{Name="Mongolia", Population= 3027000, Continent=continents[2],
                                GDP=11.16, Coordinate1 = .480, Coordinate2 = .5, Coordinate3 = .569,
                                Coordinate4 = .546
                            },
                            new City{Name="China", Population=1379000000,//1.379 Billion
                                GDP=11795.297,
                                Continent = continents[2]
                            },
                            new City{Name="Middle East", Population=7200000000,//7.2 Billion
                                GDP= 3867.415,
                                Continent = continents[2]
                            },
                            new City{Name="Germany", Population=82670000, Continent = continents[4],
                                GDP=3467
                            },
                            new City{Name="Spain", Population=46560000, Continent = continents[4],
                                GDP = 1232
                            },
                            new City{Name="Egypt", Population=95690000, Continent = continents[3],
                                GDP = 336.3
                            },
                            new City{Name="Japan", Population=127000000,//127 Million
                                GDP = 4939,
                                Continent = continents[5]
                            },
                            new City{Name="Philippines", Population=103300000,//103.3million
                                GDP = 304.9,
                                Continent = continents[5]
                            },
                            new City{Name="Indonesia", Population=261100000,
                                GDP = 932.3,
                                Continent = continents[5]
                            },
                            new City{Name="Australia", Population=24130000,
                                GDP=1205,
                                Continent = continents[5]
                            },
                            new City{Name="Rio Grande", Population=80000,
                                GDP=.01,
                                Continent = continents[1]
                            },
                            new City{Name="Primavera", Population=1016,
                                GDP = .002,
                                Continent = continents[1]
                            },
                            new City{Name="Greenland ", Population=56186, Continent = continents[0],
                                GDP = 2.22 }     
                          );
                            context.SaveChanges();  }}}  

                        

                        cities[8].Coordinate1 = .417;
                        cities[8].Coordinate2 = .436;
                        cities[8].Coordinate3 = .617;
                        cities[8].Coordinate4 = .592;

                        cities[9].Coordinate1 = .209;
                        cities[9].Coordinate2 = .229;
                        cities[9].Coordinate3 = .183;
                        cities[9].Coordinate4 = .153;

                        cities[10].Coordinate1 = .425;
                        cities[10].Coordinate2 = .461;
                        cities[10].Coordinate3 = .666;
                        cities[10].Coordinate4 = .627;

                        cities[11].Coordinate1 = .612;
                        cities[11].Coordinate2 = .653;
                        cities[11].Coordinate3 = .680;
                        cities[11].Coordinate4 = .642;

                        cities[12].Coordinate1 = .653;
                        cities[12].Coordinate2 = .676;
                        cities[12].Coordinate3 = .768;
                        cities[12].Coordinate4 = .741;

                        cities[13].Coordinate1 = .167;
                        cities[13].Coordinate2 = .212;
                        cities[13].Coordinate3 = .498;
                        cities[13].Coordinate4 = .442;

                        cities[14].Coordinate1 = .229;
                        cities[14].Coordinate2 = .249;
                        cities[14].Coordinate3 = .6;
                        cities[14].Coordinate4 = .579;

                        cities[15].Coordinate1 = .229;
                        cities[15].Coordinate2 = .275;
                        cities[15].Coordinate3 = .763;
                        cities[15].Coordinate4 = .707;

                        cities[16].Coordinate1 = .113;
                        cities[16].Coordinate2 = .147;
                        cities[16].Coordinate3 = .774;
                        cities[16].Coordinate4 = .737;

                        cities[17].Coordinate1 = .189;
                        cities[17].Coordinate2 = .206;
                        cities[17].Coordinate3 = .773;
                        cities[17].Coordinate4 = .750;

                        cities[18].Coordinate1 = .550;
                        cities[18].Coordinate2 = .572;
                        cities[18].Coordinate3 = .657;
                        cities[18].Coordinate4 = .634;

                        cities[19].Coordinate1 = .285;
                        cities[19].Coordinate2 = .334;
                        cities[19].Coordinate3 = .965;
                        cities[19].Coordinate4 = .902;


                        context.Cities.UpdateRange();
                        context.SaveChanges();}}}

                    
                        context.Products.AddRange(
                           new Product {
                               productName="Industrial Machinery",
                                Category = "Capital"
                           },new Product {
                               productName = "Consumer Automobiles",
                               Category = "Product"
                           },new Product {
                               productName = "Raw Plastic",
                               Category = "Capital"
                           },new Product {
                               productName = "Packaged Meat",
                               Category = "Product"
                           },new Product {
                               productName = "Software Development",
                               Category = "Service"
                           }, new Product {
                               productName = "Software Services",
                               Category = "Service"
                           }, new Product {
                               productName = "Industrial HVAC Equipment",
                               Category = "Capital"
                           }, new Product {
                               productName = "Misc. Fruits & Nuts",
                               Category = "Product"
                           }, new Product {
                               productName = "Consumer-Grade Medical Supplies",
                               Category = "Product"
                           }, new Product {
                               productName = "Crude Petroleum",
                               Category = "Resource"
                           }, new Product {
                               productName = "Refined Petroleum",
                               Category = "Product"
                           }, new Product {
                               productName = "Coal",
                               Category = "Capital"
                           }, new Product {
                               productName = "Petroleum Gas",
                               Category = "Product"
                           }, new Product {
                               productName = "Industrial Chemicals",
                               Category = "Capital"
                           }, new Product {
                               productName = "Fertilizer",
                               Category = "Capital"
                           }, new Product {
                               productName = "Tractors",
                               Category = "Capital"
                           }, new Product {
                               productName = "Motion Picture Entertainment",
                               Category = "Product"
                           }, new Product {
                               productName = "Pharmaceuticals",
                               Category = "Capital"
                           }, new Product {
                               productName = "Clothing",
                               Category = "Product"
                           }, new Product {
                               productName = "Computers",
                               Category = "Product"
                           }, new Product {
                               productName = "Gas Turbines",
                               Category = "Capital"
                           }, new Product {
                               productName = "Gold",
                               Category = "Resource"
                           }, new Product {
                               productName = "Integrated Circuits",
                               Category = "Capital"
                           }, new Product {
                               productName = "Sugar",
                               Category = "Product"
                           }, new Product {
                               productName = "Nickel",
                               Category = "Resource"
                           }, new Product {
                               productName = "Tobacco",
                               Category = "Product"
                           }, new Product {
                               productName = "Seafood",
                               Category = "Product"
                           }, new Product {
                               productName = "Coffee",
                               Category = "Product"
                           }, new Product {
                               productName = "Iron Ore",
                               Category = "Resource"
                           }, new Product {
                               productName = "Diamonds",
                               Category = "Resource"
                           }, new Product {
                               productName = "Manual Labor",
                               Category = "Service"
                           }
                        );
                        context.SaveChanges();      
             */           
            CityProduct[] cps = new CityProduct[cities.Count() * products.Count()];
            Random r = new Random();
            int k = 0;
            for(int i = 0; i < cities.Count(); i++) {
                for(int j = 0; j < products.Count(); j++) {
                    cps[k++] = new CityProduct {
                        CityId = cities[i].CityId,
                        City = cities[i],
                        Product = products[j],
                        ProductId = products[j].productId,
                        Type = r.Next(0, 7),
                        CurrentSurplus = r.Next(0, 10000),
                        MaximumPotentialUnitProduction = r.Next(9000, 10000),
                        ScheduledAnnualUnitProduction = r.Next(0, 9000),
                        PerUnitLocalValue = r.Next(100, 10000),
                        PerUnitProductionPrice = r.Next(10, 10000)
                    };
                }
            }
            context.AddRange(cps);
            context.SaveChanges();
            
        }         
    }
}
