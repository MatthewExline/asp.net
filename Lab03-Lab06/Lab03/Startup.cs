﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab03.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Lab03 {
    public class Startup {
        IConfigurationRoot Configuration;
        public Startup(IHostingEnvironment env) {
            Configuration=new ConfigurationBuilder().SetBasePath(env.ContentRootPath).AddJsonFile("appsettings.json").Build();
        }

        public void ConfigureServices(IServiceCollection services) {
            services.AddMvc();
            services.AddDbContext<DBContext>(
                options => options.UseSqlServer(
                    Configuration["Data:GlobalEcon:ConnectionString"]));
            services.AddLogging();            
            services.AddTransient<iCityRepo, EFCityRepo>();
            services.AddTransient<iProductRepo, EFProductRepo>();
            services.AddTransient<iContinentRepo, EFContinentRepo>();
            services.AddTransient<iCityProductRepo, EFCityProductRepo>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            app.UseDeveloperExceptionPage();
            app.UseStaticFiles();
            app.UseMvc(
                routes => {
                    //basically url-rewriting, making new uri
                    routes.MapRoute(
                        name: null,
                        template: "(type)/Page{pageNumber:int}",
                        defaults: new { controller = "Title", action = "List" }
                    );
                    routes.MapRoute(
                        name: "pagination",
                        template: "Products/Page{pageNumber}",
                        defaults: new { Controller = "Product", action = "List" }
                    );
                    routes.MapRoute(
                        name: "default",
                        template: "{controller=Product}/{action=List}/{id?}"
                    );
                    routes.MapRoute(
                        name: null,
                        template: "",
                        defaults: new { controller = "Home", action = "Index" }
                    );
                }
            );
            //MySeeds.Populate(app);
        }
    }
}
