﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models {
    public class SimpleCity {
        public int CityId { get; set; }
        public String Name {get; set;}        
        public int ContinentId{get; set;}
        public String ContinentName{get; set;}
        public double Coordinate1 { get; set; }
        public double Coordinate2 { get; set; }
        public double Coordinate3 { get; set; }
        public double Coordinate4 { get; set; }
        public double GDP { get; set; }
        public long Population { get; set; }
        public string[] ProductsImported { get; set; }
        public string[] ProductsExported { get; set; }
        /* The reason that I have a ProductsImported and a ProductsExported
         * and a ProductsImported2 and a ProductsExported2 is because of
         * the JavaScript serialization that I use in the Index Page.
         * Parsing an array of strings or ints works fine, (which I need)
         * but parsing an array of a C# class into JavaScript doesn't seem
         * to work, so I have ProductsImported as an array of Strings for
         * my JavaScript, and the '2' versions which hold more info for my 
         * C# scripts. I may add additional info besides 'name' and 'id' in
         * the future.
         * */
        public ProductInfo[] ProductsImported2{get; set;}
        public ProductInfo[] ProductsExported2{get; set;}
        public class ProductInfo {
            public ProductInfo(string name, int id) {
                ProductName = name;
                ProductId = id;
            }
            public string ProductName{get; set;}
            public int ProductId{get; set;}
        }
    }
}
