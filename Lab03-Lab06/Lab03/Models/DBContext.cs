﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models  {
    public class DBContext : DbContext {
        public DBContext(
            DbContextOptions<DBContext>options) : base(options) {}

        
        public DbSet<City> Cities{get; set;}
        public DbSet<Continent> Continents { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<CityProduct> CityProducts { get; set; }
        
        protected override void OnModelCreating(ModelBuilder mB) {
            mB.Entity<CityProduct>()
                .HasKey(t => new {t.CityId, t.ProductId, t.Type});
            
            mB.Entity<CityProduct>()
                .HasOne(p => p.Product)
                .WithMany(c => c.Cities)
                .HasForeignKey(p => p.ProductId);

            mB.Entity<CityProduct>()
                .HasOne(c => c.City)
                .WithMany(p => p.Products)
                .HasForeignKey(c => c.CityId);
                
        }
    }
}
