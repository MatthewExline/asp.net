﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace Lab03.Models {
    public static class MySeeds {
        public static void Populate(IApplicationBuilder app) {
            
            DBContext context = app.ApplicationServices.GetRequiredService<DBContext>();
            //List<Continent> continents = context.Continents.ToList();
            //context.ChangeTracker.QueryTrackingBehavior = Microsoft.EntityFrameworkCore.QueryTrackingBehavior.NoTracking;
            List<Product> products = context.Products.ToList();
            List<City> cities = context.Cities.ToList();

            /*
            List<City> cities = context.Cities.ToList();
            cities[0].Coordinate1 = .581;
            cities[0].Coordinate2 = .638;
            cities[0].Coordinate3 = .818;
            cities[0].Coordinate4 = .738;

            cities[1].Coordinate1 = .169;
            cities[1].Coordinate2 = .186;
            cities[1].Coordinate3 = .803;
            cities[1].Coordinate4 = .782;

            cities[2].Coordinate1 = .240;
            cities[2].Coordinate2 = .286;
            cities[2].Coordinate3 = .170;
            cities[2].Coordinate4 = .111;

            cities[3].Coordinate1 = .662;
            cities[3].Coordinate2 = .814;
            cities[3].Coordinate3 = .340;
            cities[3].Coordinate4 = .202;

            cities[4].Coordinate1 = .702;
            cities[4].Coordinate2 = .754;
            cities[4].Coordinate3 = .463;
            cities[4].Coordinate4 = .400;

            cities[5].Coordinate1 = .754;
            cities[5].Coordinate2 = .808;
            cities[5].Coordinate3 = .541;
            cities[5].Coordinate4 = .474;

            cities[6].Coordinate1 = .776;
            cities[6].Coordinate2 = .886;
            cities[6].Coordinate3 = .923;
            cities[6].Coordinate4 = .755;

            cities[7].Coordinate1 = .480;
            cities[7].Coordinate2 = .5;
            cities[7].Coordinate3 = .569;
            cities[7].Coordinate4 = .546;

            cities[8].Coordinate1 = .417;
            cities[8].Coordinate2 = .436;
            cities[8].Coordinate3 = .617;
            cities[8].Coordinate4 = .592;

            cities[9].Coordinate1 = .209;
            cities[9].Coordinate2 = .229;
            cities[9].Coordinate3 = .183;
            cities[9].Coordinate4 = .153;

            cities[10].Coordinate1 = .425;
            cities[10].Coordinate2 = .461;
            cities[10].Coordinate3 = .666;
            cities[10].Coordinate4 = .627;

            cities[11].Coordinate1 = .612;
            cities[11].Coordinate2 = .653;
            cities[11].Coordinate3 = .680;
            cities[11].Coordinate4 = .642;

            cities[12].Coordinate1 = .653;
            cities[12].Coordinate2 = .676;
            cities[12].Coordinate3 = .768;
            cities[12].Coordinate4 = .741;

            cities[13].Coordinate1 = .167;
            cities[13].Coordinate2 = .212;
            cities[13].Coordinate3 = .498;
            cities[13].Coordinate4 = .442;

            cities[14].Coordinate1 = .229;
            cities[14].Coordinate2 = .249;
            cities[14].Coordinate3 = .6;
            cities[14].Coordinate4 = .579;

            cities[15].Coordinate1 = .229;
            cities[15].Coordinate2 = .275;
            cities[15].Coordinate3 = .763;
            cities[15].Coordinate4 = .707;

            cities[16].Coordinate1 = .113;
            cities[16].Coordinate2 = .147;
            cities[16].Coordinate3 = .774;
            cities[16].Coordinate4 = .737;

            cities[17].Coordinate1 = .189;
            cities[17].Coordinate2 = .206;
            cities[17].Coordinate3 = .773;
            cities[17].Coordinate4 = .750;

            cities[18].Coordinate1 = .550;
            cities[18].Coordinate2 = .572;
            cities[18].Coordinate3 = .657;
            cities[18].Coordinate4 = .634;

            cities[19].Coordinate1 = .285;
            cities[19].Coordinate2 = .334;
            cities[19].Coordinate3 = .965;
            cities[19].Coordinate4 = .902;


            context.Cities.UpdateRange();
            context.SaveChanges();
            */

            /*
            context.Continents.AddRange(
                    new Continent {
                        Name="North America"
                    }, new Continent {
                        Name = "South America"
                    }, new Continent {
                        Name = "Asia"
                    }, new Continent {
                        Name = "Africa"
                    }, new Continent {
                        Name = "Europe"
                    }, new Continent {
                        Name = "Pacific"
                    }
                );
            context.SaveChanges();
            */

            /*
            context.Cities.AddRange(
                new City {
                    Name = "Russia", Population = 144300000, Continent = continents[2],
                    Coordinates = new double[4] { .581, .638, .818, .738 }, GDP = 1283
                },
                new City{Name="North Dakota",
                    Coordinates= new double[4]{.169, .186, .803, .782}, Population=757952,
                    GDP=27.72, Continent = continents[0]
                },
                new City{Name="La Crosse", Continent = continents[0],
                    Coordinates =new double[4]{.189, .206, .773, .750}, Population=52109,
                    GDP= 6.321
                },
                new City{Name="West Coast", Population=50630000,//50.6 Million
                    GDP= 3100,Coordinates=new double[4]{.113, .147, .774, .737}, Continent = continents[0]
                },
                new City{Name="East Coast", Population=112600000,//112 Million
                    Coordinates=new double[4]{.229, .275, .763, .707}, GDP = 4500, Continent = continents[0]
                },
                new City{Name="Cuba", Population= 11480000,//11.48 Million
                    Coordinates=new double[4]{.229, .249, .600, .579}, GDP = 87.13, Continent = continents[0]
                },
                new City{Name="Mexico", Population=127500000, //127.5 Million
                    Coordinates=new double[4]{.167, .212, .498, .442}, GDP = 1046.92,//gdp is in billions.
                    Continent = continents[1]
                },                
                new City{Name="Mongolia", Population= 3027000, Continent=continents[2],
                    Coordinates=new double[4]{.653, .676, .768, .741}, GDP=11.16
                },
                new City{Name="China", Population=1379000000,//1.379 Billion
                    Coordinates=new double[4]{.612, .653, .680, .642}, GDP=11795.297,
                    Continent = continents[2]
                },
                new City{Name="Middle East", Population=7200000000,//7.2 Billion
                    Coordinates=new double[4]{.550, .572, .657, .634}, GDP= 3867.415,
                    Continent = continents[2]
                },
                new City{Name="Germany", Population=82670000, Continent = continents[4],
                    Coordinates =new double[4]{.425, .461, .666, .627}, GDP=3467
                },
                new City{Name="Spain", Population=46560000, Continent = continents[4],
                    Coordinates =new double[4]{.417, .436, .617, .592}, GDP = 1232
                },
                new City{Name="Egypt", Population=95690000, Continent = continents[3],
                    Coordinates =new double[4]{.480, .500, .569, .546}, GDP = 336.3
                },
                new City{Name="Japan", Population=127000000,//127 Million
                    Coordinates=new double[4]{.776, .886, .923, .755}, GDP = 4939,
                    Continent = continents[5]
                },
                new City{Name="Philippines", Population=103300000,//103.3million
                    Coordinates=new double[4]{.754, .808, .541, .474}, GDP = 304.9,
                    Continent = continents[5]
                },
                new City{Name="Indonesia", Population=261100000,
                    Coordinates=new double[4]{.702, .754, .463, .400}, GDP = 932.3,
                    Continent = continents[5]
                },
                new City{Name="Australia", Population=24130000,
                    Coordinates=new double[4]{.662, .814, .340, .202}, GDP=1205,
                    Continent = continents[5]
                },
                new City{Name="Rio Grande", Population=80000,
                    Coordinates=new double[4]{.240, .286, .170, .111}, GDP=.01,
                    Continent = continents[1]
                },
                new City{Name="Primavera", Population=1016,
                    Coordinates=new double[4]{.209, .229, .183, .153}, GDP = .002,
                    Continent = continents[1]
                },
                new City{Name="Greenland ", Population=56186, Continent = continents[0],
                    Coordinates =new double[4]{.285, .334, .965, .902}, GDP = 2.22 }     
              );
                context.SaveChanges();
            
            context.Products.AddRange(
               new Product {
                   productName="Misc. Machinery"
               },new Product {
                   productName = "Misc. Automobiles"
               },new Product {
                   productName = "Plastic"
               },new Product {
                   productName = "Misc. Meat"
               },new Product {
                   productName = "Software Developers"
               }, new Product {
                   productName = "Software Services"
               }, new Product {
                   productName = "HVAC Equipment"
               }, new Product {
                   productName = "Misc. Fruits & Nuts"
               }, new Product {
                   productName = "Misc. Medical"
               }, new Product {
                   productName = "Crude Petroleum"
               }, new Product {
                   productName = "Refined Petroleum"
               }, new Product {
                   productName = "Coal"
               }, new Product {
                   productName = "Petroleum Gas"
               }, new Product {
                   productName = "Industrial Chemicals"
               }, new Product {
                   productName = "Fertilizer"
               }, new Product {
                   productName = "Tractors"
               }, new Product {
                   productName = "Motion Picture Entertainment"
               }, new Product {
                   productName = "Pharmaceuticals"
               }, new Product {
                   productName = "Clothing"
               }, new Product {
                   productName = "Automotive Parts"
               }, new Product {
                   productName = "Computers"
               }, new Product {
                   productName = "Computer Parts"
               }, new Product {
                   productName = "Gas Turbines"
               }, new Product {
                   productName = "Gold"
               }, new Product {
                   productName = "Soybeans"
               }, new Product {
                   productName = "Integrated Circuits"
               }, new Product {
                   productName = "Sugar"
               }, new Product {
                   productName = "Nickel"
               }, new Product {
                   productName = "Tobaco"
               }, new Product {
                   productName = "Shellfish"
               }, new Product {
                   productName = "Coffee"
               }, new Product {
                   productName = "Recyclable Petrol Products"
               }, new Product {
                   productName = "Luxury foods"
               }, new Product {
                   productName = "Iron Ore"
               }, new Product {
                   productName = "Fish & Crustaceans"
               }, new Product {
                   productName = "Knit Clothing"
               }, new Product {
                   productName = "Non-food Animal Products"
               }, new Product {
                   productName = "Vegetable Oils"
               }, new Product {
                   productName = "Diamonds & Jewellery"
               }, new Product {
                   productName = "EVERYTHING"
               }, new Product {
                   productName = "Numerous Metal Ores"
               }
            );
            context.SaveChanges();
            */
            
            /*
            context.CityProducts.AddRange(
                new CityProduct {
                    Type = 1,//east coast exports
                    City = cities[15], CityId = cities[15].CityId,
                    Product = products[22],
                    ProductId = products[22].productId
                }, new CityProduct {
                    Type = 1,//east coast exports
                    City = cities[15], CityId = cities[15].CityId,
                    Product = products[31],
                    ProductId = products[31].productId
                }, new CityProduct {
                    Type = 1,//east coast exports
                    City = cities[15], CityId = cities[15].CityId,
                    Product = products[0],
                    ProductId = products[0].productId
                }, new CityProduct {
                    Type = 1,//east coast exports
                    City = cities[15], CityId = cities[15].CityId,
                    Product = products[38],
                    ProductId = products[38].productId
                }, new CityProduct {
                    Type = 1,//cuba exports
                    City = cities[14], CityId = cities[14].CityId,
                    Product = products[7],
                    ProductId = products[7].productId
                }, new CityProduct {
                    Type = 1,//cuba exports
                    City = cities[14], CityId = cities[14].CityId,
                    Product = products[5],
                    ProductId = products[5].productId
                }, new CityProduct {
                    Type = 1,//japan exports
                    City = cities[6], CityId = cities[6].CityId,
                    Product = products[6],
                    ProductId = products[6].productId
                }, new CityProduct {
                    Type = 1,//japan exports
                    City = cities[6], CityId = cities[6].CityId,
                    Product = products[22],
                    ProductId = products[22].productId
                }, new CityProduct {
                    Type = 1,//greenland exports
                    City = cities[19], CityId = cities[19].CityId,
                    Product = products[6],
                    ProductId = products[6].productId
                }
                /*
                new CityProduct {
                    Type=1,
                    City=cities[0],
                    CityId=cities[0].CityId,
                    Product=products[30],
                    ProductId=products[30].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[0],
                    CityId = cities[0].CityId,
                    Product = products[31],
                    ProductId = products[31].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[0],
                    CityId = cities[0].CityId,
                    Product = products[32],
                    ProductId = products[32].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[0],
                    CityId = cities[0].CityId,
                    Product = products[33],
                    ProductId = products[33].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[1],
                    CityId = cities[1].CityId,
                    Product = products[30],
                    ProductId = products[30].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[1],
                    CityId = cities[1].CityId,
                    Product = products[36],
                    ProductId = products[36].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[17],
                    CityId = cities[17].CityId,
                    Product = products[25],
                    ProductId = products[25].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[17],
                    CityId = cities[17].CityId,
                    Product = products[27],
                    ProductId = products[27].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[16],//west coast
                    CityId = cities[16].CityId,
                    Product = products[26],//softwareServices
                    ProductId = products[26].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[16],//west coast
                    CityId = cities[16].CityId,
                    Product = products[37],//motion picture
                    ProductId = products[37].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[16],//west coast
                    CityId = cities[16].CityId,
                    Product = products[28],
                    ProductId = products[28].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[16],//west coast
                    CityId = cities[16].CityId,
                    Product = products[24],
                    ProductId = products[24].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[4],
                    CityId = cities[4].CityId,
                    Product = products[1],
                    ProductId = products[1].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[4],
                    CityId = cities[4].CityId,
                    Product = products[2],
                    ProductId = products[2].productId
                }, new CityProduct {
                    Type=1,
                    City = cities[4],
                    CityId = cities[4].CityId,
                    Product = products[3],
                    ProductId = products[3].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[4],
                    CityId = cities[4].CityId,
                    Product = products[4],
                    ProductId = products[4].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[4],
                    CityId = cities[4].CityId,
                    Product = products[22],//misc Automoible
                    ProductId = products[22].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[4],
                    CityId = cities[4].CityId,
                    Product = products[29],//misc medical
                    ProductId = products[29].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[5],
                    CityId = cities[5].CityId,
                    Product = products[29],//misc Medical
                    ProductId = products[29].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[5],
                    CityId = cities[5].CityId,
                    Product = products[5],//
                    ProductId = products[5].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[5],
                    CityId = cities[5].CityId,
                    Product = products[6],//
                    ProductId = products[6].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[5],
                    CityId = cities[5].CityId,
                    Product = products[7],//
                    ProductId = products[7].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[5],
                    CityId = cities[5].CityId,
                    Product = products[8],//
                    ProductId = products[8].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[5],
                    CityId = cities[5].CityId,
                    Product = products[9],//
                    ProductId = products[9].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[19],//greenland
                    CityId = cities[19].CityId,
                    Product = products[11],//
                    ProductId = products[11].productId
                },
                new CityProduct {
                    Type=1,
                    City = cities[4],
                    CityId = cities[4].CityId,
                    Product = products[13],//computers
                    ProductId = products[13].productId
                },
                new CityProduct {//Russia
                    Type= 0,
                    City = cities[0], CityId = cities[0].CityId,
                    Product = products[0],//misc machinery
                    ProductId = products[0].productId
                }, new CityProduct {//Russia
                    Type = 0,
                    City = cities[0], CityId = cities[0].CityId,
                    Product = products[22],//misc automobiles
                    ProductId = products[22].productId
                }, new CityProduct {//Russia
                    Type = 0,
                    City = cities[0], CityId = cities[0].CityId,
                    Product = products[29],//misc medical
                    ProductId = products[29].productId
                }, new CityProduct {//Russia
                    Type = 0,
                    City = cities[0], CityId = cities[0].CityId,
                    Product = products[23],//plastic
                    ProductId = products[23].productId
                }, new CityProduct {//Russia
                    Type = 0,
                    City = cities[0], CityId = cities[0].CityId,
                    Product = products[24],//misc meat
                    ProductId = products[24].productId
                }, new CityProduct {//Russia
                    Type = 0,
                    City = cities[0], CityId = cities[0].CityId,
                    Product = products[28],//misc fruits & nuts
                    ProductId = products[28].productId
                }, new CityProduct {//N Dakota
                    Type = 0,
                    City = cities[1], CityId = cities[1].CityId,
                    Product = products[39],
                    ProductId = products[39].productId
                }, new CityProduct {//N Dakota
                    Type = 0,
                    City = cities[1], CityId = cities[1].CityId,
                    Product = products[35],//
                    ProductId = products[35].productId
                }, new CityProduct {//N Dakota
                    Type = 0,
                    City = cities[1], CityId = cities[1].CityId,
                    Product = products[1],
                    ProductId = products[1].productId
                }, new CityProduct {//N Dakota
                    Type = 0,
                    City = cities[1], CityId = cities[1].CityId,
                    Product = products[33],
                    ProductId = products[33].productId
                }, new CityProduct {//La Crosse
                    Type = 0,
                    City = cities[17], CityId = cities[17].CityId,
                    Product = products[31],//
                    ProductId = products[31].productId
                }, new CityProduct {//La Crosse
                    Type = 0,
                    City = cities[17], CityId = cities[17].CityId,
                    Product = products[16],//
                    ProductId = products[16].productId
                }, new CityProduct {//La Crosse
                    Type = 0,
                    City = cities[17], CityId = cities[17].CityId,
                    Product = products[7],
                    ProductId = products[7].productId
                }, new CityProduct {//La Crosse
                    Type = 0,
                    City = cities[17], CityId = cities[17].CityId,
                    Product = products[9],//
                    ProductId = products[9].productId
                }, new CityProduct {//La Crosse
                    Type = 0,
                    City = cities[17], CityId = cities[17].CityId,
                    Product = products[34],//
                    ProductId = products[34].productId
                }, new CityProduct {//La Crosse
                    Type = 0,
                    City = cities[17], CityId = cities[17].CityId,
                    Product = products[28],//
                    ProductId = products[28].productId
                }, new CityProduct {//West Coast
                    Type = 0,
                    City = cities[16], CityId = cities[16].CityId,
                    Product = products[25],//Software Developers
                    ProductId = products[25].productId
                }, new CityProduct {//West Coast
                    Type = 0,
                    City = cities[16], CityId = cities[16].CityId,
                    Product = products[27],//
                    ProductId = products[27].productId
                }, new CityProduct {//West Coast
                    Type = 0,
                    City = cities[16], CityId = cities[16].CityId,
                    Product = products[31],//
                    ProductId = products[31].productId
                }, new CityProduct {//West Coast
                    Type = 0,
                    City = cities[16], CityId = cities[16].CityId,
                    Product = products[22],//
                    ProductId = products[22].productId
                }, new CityProduct {//East Coast
                    Type = 0,
                    City = cities[4], CityId = cities[4].CityId,
                    Product = products[39],//Automotive Parts
                    ProductId = products[39].productId
                }, new CityProduct {//East Coast
                    Type = 0,
                    City = cities[4], CityId = cities[4].CityId,
                    Product = products[29],//misc medical
                    ProductId = products[29].productId
                }, new CityProduct {//East Coast
                    Type = 0,
                    City = cities[4], CityId = cities[4].CityId,
                    Product = products[21],//clothing
                    ProductId = products[21].productId
                }, new CityProduct {//East Coast
                    Type = 0,
                    City = cities[4], CityId = cities[4].CityId,
                    Product = products[18],//computer parts
                    ProductId = products[18].productId
                }, new CityProduct {//East Coast
                    Type = 0,
                    City = cities[4], CityId = cities[4].CityId,
                    Product = products[20],//computers
                    ProductId = products[20].productId
                }, new CityProduct {//Cuba
                    Type = 0,
                    City = cities[5], CityId = cities[5].CityId,
                    Product = products[31],//refined petrol
                    ProductId = products[31].productId
                }, new CityProduct {//Cuba
                    Type = 0,
                    City = cities[5], CityId = cities[5].CityId,
                    Product = products[24],//misc meat
                    ProductId = products[24].productId
                }, new CityProduct {//Cuba
                    Type = 0,
                    City = cities[5], CityId = cities[5].CityId,
                    Product = products[0],//misc machinery
                    ProductId = products[0].productId
                }, new CityProduct {//Cuba
                    Type = 0,
                    City = cities[5], CityId = cities[5].CityId,
                    Product = products[34],//industrial chems
                    ProductId = products[34].productId
                }, new CityProduct {//Greenland
                    Type = 0,
                    City = cities[19], CityId = cities[19].CityId,
                    Product = products[31],//refined petrol
                    ProductId = products[31].productId
                }, new CityProduct {//Greenland
                    Type = 0,
                    City = cities[19], CityId = cities[19].CityId,
                    Product = products[0],//misc machinery
                    ProductId = products[0].productId
                }, new CityProduct {//Greenland
                    Type = 0,
                    City = cities[19], CityId = cities[19].CityId,
                    Product = products[11],//luxury food
                    ProductId = products[11].productId
                }, new CityProduct {
                    Type=0,
                    City = cities[6], CityId=cities[6].CityId,
                    Product = products[0],
                    ProductId=products[0].productId
                }, new CityProduct {
                    Type = 0,
                    City = cities[6], CityId = cities[6].CityId,
                    Product = products[30],
                    ProductId = products[30].productId
                }, new CityProduct {
                    Type = 0,
                    City = cities[6], CityId = cities[6].CityId,
                    Product = products[31],
                    ProductId = products[31].productId
                }, new CityProduct {
                    Type = 0,
                    City = cities[6], CityId = cities[6].CityId,
                    Product = products[33],
                    ProductId = products[33].productId
                }, new CityProduct {
                    Type = 0,//egypt imports
                    City = cities[7], CityId = cities[7].CityId,
                    Product = products[22],
                    ProductId = products[22].productId
                }, new CityProduct {
                    Type = 0,//egypt imports
                    City = cities[7], CityId = cities[7].CityId,
                    Product = products[31],
                    ProductId = products[31].productId
                }, new CityProduct {
                    Type = 0,//spain imports
                    City = cities[8], CityId = cities[8].CityId,
                    Product = products[30],
                    ProductId = products[30].productId
                }, new CityProduct {
                    Type = 0,//spain imports
                    City = cities[8], CityId = cities[8].CityId,
                    Product = products[22],
                    ProductId = products[22].productId
                }, new CityProduct {
                    Type = 0,//spain imports
                    City = cities[8], CityId = cities[8].CityId,
                    Product = products[29],
                    ProductId = products[29].productId
                }
            );
            context.SaveChanges();*/
        }         
    }
}

