﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models{
    public class City{
        public int CityId{get; set;}
        public string Name{get; set;}
        public double GDP{get; set;}
        public long Population{get; set;}
        public double Coordinate1 {get; set;}
        public double Coordinate2 { get; set; }
        public double Coordinate3 { get; set; }
        public double Coordinate4 { get; set; }
        public List<CityProduct> Products{get; set;}

        public int ContinentId{get; set;}
        public Continent Continent {get; set;}
        /*
        public void fixCoordinates() {
            Coordinate1 = Coordinates[1];
            Coordinate2 = Coordinates[2];
            Coordinate3 = Coordinates[3];
            Coordinate4 = Coordinates[4];
        }
        
        public bool Within(double x, double y){
            if(Coordinates[1] <= x && Coordinates[2] >= x
            && Coordinates[3] <= y && Coordinates[4] >= y) 
                return true;
            else return false;
        }        */
    }
}
