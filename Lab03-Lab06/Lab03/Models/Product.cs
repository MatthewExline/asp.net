﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models {
    public class Product {
        public int productId{get; set;}
        public String productName{get; set;}
        public List<CityProduct>Cities{get; set;}
    }
}
