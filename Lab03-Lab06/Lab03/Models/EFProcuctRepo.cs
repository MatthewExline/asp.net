﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models {
    public class EFProductRepo : iProductRepo {
        public DBContext context;
        public IEnumerable<Product> Products => context.Products;

        public EFProductRepo(DBContext context) {
            this.context = context;
        }
    }
}