﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models.ViewModels {
    public class IndexViewModel {
        public SimpleCity[] SimpleCities{get; set;}
        public City[] Cities{get; set;}
    }
}
