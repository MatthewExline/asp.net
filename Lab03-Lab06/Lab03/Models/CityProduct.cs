﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models {
    
    public class CityProduct {
        //public int CityProductExportId { get; set; }
        //Our example has: public int AuthorOrder{get;set;}  but I don't know how that would be implemented here or if it's necessary?

        public City City { get; set; }
        public int CityId { get; set; }

        public Product Product { get; set; }
        public int ProductId { get; set; }

        public int Type{get; set;}//0:import or 1:export
    }
    
}
