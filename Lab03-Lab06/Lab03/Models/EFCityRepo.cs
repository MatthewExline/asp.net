﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models {
    public class EFCityRepo : iCityRepo {
        public DBContext context;
        public IEnumerable<City>Cities => context.Cities;

        public EFCityRepo(DBContext context) {
            this.context=context;
        }
    }
}
