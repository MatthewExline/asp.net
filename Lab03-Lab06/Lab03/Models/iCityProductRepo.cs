﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models {
    public interface iCityProductRepo {
        IEnumerable<City> Cities { get; }
        IEnumerable<Product> Products { get; }
        IEnumerable<CityProduct>CityProducts{get;}
    }
}
