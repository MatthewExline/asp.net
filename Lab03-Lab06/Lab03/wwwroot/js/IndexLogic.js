﻿var cities;
var currentCity;
var highlightCities = false;
var cityAreas = null;
var mouseOffofMap = false;
document.addEventListener(onload, function () {
    initializeCoordinates();
    var cityPanel = document.getElementById("cityPanel");
});

document.addEventListener("click", function (e) {
    if (document.activeElement.type != "button" && document.activeElement.type != "option" &&
        document.activeElement.type != "select-one" && document.activeElement.type != "select"
        && !mouseOffofMap) { //mouseOffofMap is set true/false in the _WorldEconomyPanel partial class.
        //var x = (e.pageX + document.body.offsetLeft) / document.body.scrollWidth;
        var x = e.pageX / document.body.scrollWidth;        
        //var y = (e.pageY + document.body.offsetTop) / document.body.scrollHeight;
        var y = (document.body.scrollHeight - e.pageY) / document.body.scrollHeight;//scrollHeight, offsetHeight, clientHeight
        //types of x's and y's for mouseclick events: client, offset, page, screen, regular
        var clickedOnACity = false; 
        cities.forEach(function (c) {
            if (c.coordinates == undefined) initializeCoordinates();
            if (within(x, y, c.coordinates)) {
                currentCity = c;
                setPanel(c);
                clickedOnACity = true;
            }      
        });
        if (!clickedOnACity) { //alert("x: " + x + "   y: " + y);
            document.getElementById("cityPanel").innerHTML = "x: " + x.toFixed(3) + "   y: " + y.toFixed(3);
        }
    }
});
function initializeCoordinates() {
    for (var i = 0; i < cities.length; i++) {
        cities[i].coordinates = [];
        cities[i].coordinates.push(cities[i].coordinate1);
        cities[i].coordinates.push(cities[i].coordinate2);
        cities[i].coordinates.push(cities[i].coordinate3);
        cities[i].coordinates.push(cities[i].coordinate4);
    }
}
function toggleCityHighlights() {
    //if (c.coordinates == undefined) initializeCoordinates();
    if (!highlightCities) {//if we're turning the highlights on...
        document.getElementById("cityHighlighter").innerText = "Hide Highlights...";
        if (cityAreas == null) {
            //if the cityAreas don't exist yet (aka it's null) then we build it
            cities.forEach(function (c) {
                var newDiv = document.createElement("div");
                newDiv.setAttribute("id", "cityArea" + c.CityId);
                newDiv.setAttribute("class", "cityArea");
                newDiv.style.position = "absolute";
                newDiv.style.backgroundColor = "red";
                newDiv.style.zIndex = 12;
                if (c.coordinates == undefined) initializeCoordinates();
                newDiv.style.left = (c.coordinates[0] * 100) + "%";
                //the line below this comment took about 7 hours to write and I don't think I'm exaggerating.
                newDiv.style.top = document.body.scrollHeight - (c.coordinates[2] * document.body.scrollHeight) + "px";
                newDiv.style.width = ((c.coordinates[1] - c.coordinates[0]) * 100) + "%";
                newDiv.style.height = ((c.coordinates[2] * document.body.scrollHeight - c.coordinates[3] * document.body.scrollHeight)) + "px";
                document.body.appendChild(newDiv);
                cityAreas = document.querySelectorAll(".cityArea");
            });
        } else {//if it was already built then we just have to set it to visible.
            for (var i = 0; i < cityAreas.length; i++) {
                cityAreas[i].style.left = (cities[i].coordinates[0] * 100) + "%";                
                cityAreas[i].style.top = document.body.scrollHeight - (cities[i].coordinates[2] * document.body.scrollHeight) + "px";
                cityAreas[i].style.width = ((cities[i].coordinates[1] - cities[i].coordinates[0]) * 100) + "%";
                cityAreas[i].style.height = ((cities[i].coordinates[2] * document.body.scrollHeight - cities[i].coordinates[3] * document.body.scrollHeight)) + "px";
                cityAreas[i].style.display = "flex";
            }
        }
    } else {
        document.getElementById("cityHighlighter").innerText = "Highlight City Areas";
        for (var i = 0; i < cityAreas.length; i++) {
            cityAreas[i].style.display = "none";
        }
    }  
    if (!highlightCities) highlightCities = true;
    else highlightCities = false;
}
function within(x, y, Coordinates){
    if (Coordinates[0] <= x && Coordinates[1] >= x
        && Coordinates[2] >= y && Coordinates[3] <= y)
        return true;
    else {        
        return false;
    }
}
function setPanel(c) {    
    var prodsImp = "";
    for (var i = 0; i < c.productsImported.length; i++) {
        prodsImp += c.productsImported[i] + ", ";
    }
    var prodsExp = "";
    for (var i = 0; i < c.productsExported.length; i++) {
        prodsExp += c.productsExported[i] + ", ";
    }
    cityPanel.innerHTML = c.name + "<br>" +
        "Population: " + c.population + "<br>" +
        "GDP: $" + c.gdp + " Billion USD<br>" +
        "Products Imported: " + prodsImp + "<br>" +
        "Products Exported: " + prodsExp;
}
function closePanel() {
    cityPanel.style.display = "none";    
}