var acme = acme || {};
acme.ajax = {
	send: function(options){
		var http = new XMLHttpRequest();
		http.open(options.method, options.url, options.isAsynchronous);
		if(options.headers){
			for(var key in options.headers){
				//to get the value: someObject[key]
				http.setRequestHeader(key, options.headers[key]);
			}
		}
		http.onreadystatechange = function(){ //could also have used http.addEventListener
			if(http.readyState == 4 && http.status == 200){			
				if(options.isAsynchronous){
					http.addEventListener("load", options.callback(http.responseText));
				}else{
					options.callback(http.responseText);					
				}
			} else if (http.readyState == 4){
				acme.ajax.error(http.status);
			}
		}
		http.send(options.requestBody);
	},
	error: function(errorCode){
		if(errorCode == 2 || (errorCode >= 200 && errorCode < 300)){
			alert("Hurray, your request went through successfully!");
		}else if(errorCode==4 ||(errorCode>=400 && errorCode < 500)){
			alert("Something was wrong with the request.");
		}else if(errorCode==5 ||(errorCode>=500 && errorCode < 600)){
			alert("Something went wrong on the server, the request is not at fault.");
		}
	}
};