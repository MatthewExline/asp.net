﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Lab03.Models;
using Lab03.Models.ViewModels;

namespace Lab03.Controllers {
    public class CityController : Controller {
        private iCityRepo cityRepo;
        private iContinentRepo contRepo;
        private iCityProductRepo cityProductRepo;
        private iProductRepo productRepo;
        private int PageSize = 2;
        public CityController(iCityRepo cityr, iContinentRepo contr, iCityProductRepo cpr, iProductRepo pr) {
            cityRepo = cityr; contRepo = contr; cityProductRepo = cpr; productRepo = pr;
        }
        /*
        private City GetCity() {
            City city = HttpContext.Session.GetJson<City>("City") ?? new City();
            return city;
        }
        */
        
        public ViewResult ViewCity(int CityId, int importsPageNumber = 1, int exportsPageNumber = 1) {
            City city = cityRepo.Cities.FirstOrDefault(p => p.CityId == CityId);            
            ViewData.Model= city ?? throw new Exception(
                "There was a problem finding that city in the CityController...\n" +
                    "The cityId searched for was: "+CityId);
            
            CityProduct[] cityProducts = cityProductRepo.CityProducts.ToArray();
            Product[] products = productRepo.Products.ToArray();
            List<SimpleCity.ProductInfo> simpleImports = new List<SimpleCity.ProductInfo>();
            List<SimpleCity.ProductInfo> simpleExports = new List<SimpleCity.ProductInfo>();
            for(int j = 0; j < cityProducts.Length; j++) {
                if(cityProducts[j].CityId == city.CityId) {
                    for(int k = 0; k < products.Length; k++) {
                        if(cityProducts[j].ProductId == products[k].productId) {
                            if(cityProducts[j].Type == 0) {
                                SimpleCity.ProductInfo pI = new SimpleCity.ProductInfo(products[k].productName,
                                products[k].productId);
                                simpleImports.Add(pI);
                            } else {
                                SimpleCity.ProductInfo pI = new SimpleCity.ProductInfo(products[k].productName,
                                 products[k].productId);
                                simpleExports.Add(pI);
                            }
                            break;
                        }
                    }
                }
            }
            SimpleCity scity = new SimpleCity {
                ProductsImported2 = simpleImports.ToArray(),
                ProductsExported2 = simpleExports.ToArray(),
                CityId = city.CityId,
                Coordinate1 = city.Coordinate1,
                Coordinate2 = city.Coordinate2,
                Coordinate3 = city.Coordinate3,
                Coordinate4 = city.Coordinate4,
                Name = city.Name,
                Population = city.Population,
                ContinentId = city.ContinentId,
                ContinentName = contRepo.Continents.FirstOrDefault(c => c.ContinentId == city.ContinentId).Name,
                GDP = city.GDP
            };
            PagingInfo importsPages = new PagingInfo() {
                CurrentPage = importsPageNumber,
                ItemsPerPage = PageSize,
                TotalItems = scity.ProductsImported2.Length
            };
            PagingInfo exportsPages = new PagingInfo() {
                CurrentPage = exportsPageNumber,
                ItemsPerPage = PageSize,
                TotalItems = scity.ProductsExported2.Length
            };
            CityViewModel cvm = new CityViewModel{
                City = scity, ImportsPages=importsPages, ExportsPages=exportsPages
            };
            return View("CityDetails", cvm);            
        }
    }
}