﻿using BirdStore.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BirdStore {
    public class Startup {

        IConfigurationRoot Configuration;

        public Startup(IHostingEnvironment env) { 
            Configuration = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true).Build();
        }
        
        public void ConfigureServices(IServiceCollection services)   {
            services.AddMvc();

            //The @ symbol makes the string interpretted as a literal.
            //We are taking out this conn String and putting it into a JSON file.
            // var conn = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=SportsStore;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            services.AddDbContext<ApplicationDbContext>(
                options =>
                options.UseSqlServer(
                    Configuration["Data:BirdStoreBirds:ConnectionString"]));
            
            services.AddDbContext<AppIdentityDbContext>(
                OptionsConfigurationServiceCollectionExtensions=>
                OptionsConfigurationServiceCollectionExtensions
                .UseSqlServer(Configuration["Data:BirdStoreIdentity:ConnectionString"])
            );

            services.AddIdentity<IdentityUser, IdentityRole>().AddEntityFrameworkStores<AppIdentityDbContext>();


            services.AddTransient<IBirdRepository, EFBirdRepository>();
            services.AddTransient<IOrderRepository, EFOrderRepository>();
            services.AddTransient<IAbilityRepository, EFAbilityRepository>();
            services.AddTransient<IImageRepository, EFIMageRepository>();
            services.AddTransient<IBirdAbilityRepository, EFBirdAbilityRepository>();
            services.AddMemoryCache();
            services.AddSession();

            
            services.AddScoped<Cart>(sp => SessionCart.GetCart(sp));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>(); 
        }
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ApplicationDbContext context)  {
           
            if(env.IsDevelopment()){
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();
            }else{
                app.UseExceptionHandler("/Error");
            }
            app.UseStaticFiles();

            app.UseSession();
            app.UseIdentity();
            app.UseMvc(
                routes =>
                {
                    routes.MapRoute(
                        name: null,
                        template: "{category}/Page{pageNumber:int}",
                        defaults: new { Controller = "Bird", action = "List" }
                        );

                    routes.MapRoute(
                        name: null,
                        template: "Page{pageNumber:int}",
                        defaults: new { Controller = "Bird", action = "List", pageNumber = 1 }
                        );

                    routes.MapRoute(
                        name: null,
                        template: "{category}",
                        defaults: new { Controller = "Bird", action = "List", pageNumber = 1 }
                        );

                    routes.MapRoute(
                        name: null,
                        template: "",
                        defaults: new {controller="Bird", action="List", pageNumber = 1}
                        );

                    routes.MapRoute(
                        name: null,
                        template: "{controller}/{action}/{id?}"
                        );
                });
            //SeedData.EnsurePopulate(context);
        }
    }
}
