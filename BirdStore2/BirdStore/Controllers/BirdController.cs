﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BirdStore.Models;
using BirdStore.Models.ViewModels;

namespace BirdStore.Controllers {
    public class BirdController : Controller    {
        private IBirdRepository birdRepo;
        private IAbilityRepository abilityRepo;
        private IBirdAbilityRepository bar;
        private IImageRepository imageRepo;
        private int PageSize = 3; 

        public BirdController(IBirdRepository repo, IAbilityRepository ar, IBirdAbilityRepository bar, IImageRepository r) {
            birdRepo = repo; abilityRepo = ar; this.bar = bar; imageRepo = r;
        }

        public ViewResult List(string category, int pageNumber = 1) {
            IEnumerable<Bird> pagedBirds = birdRepo.Birds;
            if (category==null) {
                pagedBirds = birdRepo.Birds
                .OrderBy(p => p.BirdId)
                .Skip((pageNumber - 1) * PageSize) 
                .Take(PageSize); 
            } else {
                pagedBirds = birdRepo.Birds
                .Where(p => null == p.Category || p.Category == category)
                .OrderBy(p => p.BirdId)
                .Skip((pageNumber - 1) * PageSize)
                .Take(PageSize);
            }            
            foreach(Bird b in pagedBirds) {
                List<Ability>abilities = new List<Ability>();
                List<String> images = new List<String>();
                foreach(BirdAbility ba in bar.BirdAbilities) {
                    if(b.BirdId==ba.BirdId){
                        abilities.Add(abilityRepo.Abilities.FirstOrDefault(a=>a.AbilityId==ba.AbilityId));
                    }                        
                }
				
                foreach(Image i in imageRepo.Images)
                    if(i.BirdId == b.BirdId)
                        images.Add(i.Name);
                b.Images2 = images;
                b.Abilities2 = abilities;
            }

			BirdListViewModel model = new BirdListViewModel {
				Birds = pagedBirds
			};
			PagingInfo pInfo = new PagingInfo() {
					CurrentPage = pageNumber,
					ItemsPerPage = PageSize,
					TotalItems = null == category ?
						birdRepo.Birds.Count() :
						birdRepo.Birds
						.Where(p => p.Category == category)
						.Count()
			};
			model.CurrentCategory = category;
			model.PagingInfo = pInfo;
			return View(model);
        }
    }
}