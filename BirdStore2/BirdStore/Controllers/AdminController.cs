﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BirdStore.Models;
using BirdStore.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using System.IO;

namespace BirdStore.Controllers {

    public class AdminController : Controller {
        private IBirdRepository birdRepo;
        private IAbilityRepository abilityRepo;
        private IBirdAbilityRepository birdAbilityRepo;
		private IImageRepository imageRepo;
		private ApplicationDbContext context;
        public AdminController(IBirdRepository pRepo, IAbilityRepository abilityRepository,
			 IBirdAbilityRepository bar, ApplicationDbContext c, IImageRepository i) {
            this.birdRepo = pRepo; abilityRepo = abilityRepository; birdAbilityRepo = bar; context = c;
			this.imageRepo = i;
        }

        [Authorize]
        public IActionResult Index() {
			AdminViewModel model = new AdminViewModel {
				Images = imageRepo.Images.ToArray(),
				Birds = birdRepo.Birds.ToArray(),
				Abilities = abilityRepo.Abilities.ToArray()
			};
			return View(model);
        }

        [Authorize]
        [HttpGet]
        public ViewResult EditBird(int BirdId = 0) {
			Bird bird;
			if(BirdId==0){
				bird = new Bird();
			}else{
				bird = birdRepo.Birds.FirstOrDefault(p => p.BirdId == BirdId);
				bird = ApplyAbilitiesToBird(bird);
			}
			return View(bird);
        }
		
		[Authorize]
        [HttpGet]
        public ViewResult EditImage(int ImageId = 0) {
			Image image;
			if(ImageId==0){
				image = new Image();
			}else{
				image = imageRepo.Images.FirstOrDefault(p => p.ImageId == ImageId);
			}
			return View(image);
        }

        [Authorize]
        [HttpGet]
        public ViewResult EditAbility(int AbilityId = 0) {
            if(AbilityId == 0) {
                return View(new Ability());
            }else{            
                return View(abilityRepo.Abilities.FirstOrDefault(p=>p.AbilityId==AbilityId));
            }
        }

		[HttpGet]
		public FileStreamResult[] LoadImages(int birdId){
			Bird bird = birdRepo.Birds.FirstOrDefault(b=>b.BirdId==birdId);
			if(bird == null) return null;
			Image[] databaseImages = null;
			try{
				databaseImages = imageRepo.Images.Where(i=>i.BirdId==birdId).ToArray();
			}catch(Exception) {
				return null;
			}		
			if(databaseImages!=null && databaseImages.Length > 0){
				FileStreamResult[] FSRs = new FileStreamResult[databaseImages.Length];
				for(int i = 0; i < databaseImages.Length; i++){
					FSRs[i] = new FileStreamResult(
						new System.IO.MemoryStream(databaseImages[i].Picture),
						 databaseImages[i].ContentType/*"image / jpeg"*/
					);
				}
				return FSRs;
			} else return null;
		}

		[Authorize]
        [HttpPost]
        public async Task<IActionResult> EditImage(Image image) {
			//try{
			//	image.Picture = System.IO.File.ReadAllBytes(HttpContext.Current.Server.MapPath(image.Picture));
			//}catch(Exception){
				
			//}
			if(ModelState.IsValid){
				//image.Picture = image.Picture.ReadAllBytes();
				using(var memoryStream = new MemoryStream()){
					//await image.Picture.CopyToAsync(memoryStream);
					image.Picture = memoryStream.ToArray();
				}
				imageRepo.SaveImage(image);
                TempData["message"] = $"{image.Name} has been saved.";
                return RedirectToAction("Index");
			}else{
				TempData["message"] = $"{image.Name} has NOT been saved.";
				return View(image);
			}
		}

        [Authorize]
        [HttpPost]
        public IActionResult EditBird(Bird bird, string birdAbilityIds) {
			int[]birdAbilityIds2=null;
			if(birdAbilityIds!=null){
				birdAbilityIds2 = parseStringToInts(birdAbilityIds);
			}
            bool continueBecauseTheBirdModelStateIsOkay = true;
            foreach(
              KeyValuePair<String, Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateEntry>
              k in ModelState) {
                if(k.Key != "birdAbilityIds" && 
                  k.Value.ValidationState==Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Invalid) {
                    continueBecauseTheBirdModelStateIsOkay = false;
                }
            }
            if(continueBecauseTheBirdModelStateIsOkay) {
				birdRepo.SaveBird(bird);
				List<BirdAbility>birdAbilitiesToBeDeleted=new List<BirdAbility>();
				if(birdAbilityIds2 != null) {
					List<int>selectedIds = birdAbilityIds2.ToList();					
					foreach(BirdAbility ba in birdAbilityRepo.BirdAbilities){
						if(ba.BirdId == bird.BirdId){
							bool found = false;
							List<int>idsToRemove=new List<int>();
							foreach(int id in selectedIds){
								if(ba.AbilityId == id){
									idsToRemove.Add(id);
									//selectedIds.Remove(id);//<--I remove ints from the list.
									found = true;								
								}
							}
							foreach(int i in idsToRemove){
								selectedIds.Remove(i);
							}
							if(!found){
								birdAbilitiesToBeDeleted.Add(ba);
							}						
						}
					}
					foreach(int idToAdd in selectedIds){//This works 
						birdAbilityRepo.AddBirdAbility(bird.BirdId, idToAdd);
					}
				}else{
					foreach(BirdAbility ba in birdAbilityRepo.BirdAbilities){
						if(ba.BirdId == bird.BirdId){							
							birdAbilitiesToBeDeleted.Add(ba);
						}
					}
				}
				if(birdAbilitiesToBeDeleted.Count > 0 ){
					birdAbilityRepo.Delete(birdAbilitiesToBeDeleted);
				}		
                //birdRepo.SaveBird(bird); This can't be called so late because the bird needs to be saved
//prior to trying to create BirdAbility database objects.  Until bird is saved, it has a BirdId of 0.  
// Zero is not it's final id, it is temporary.  We need to create the BirdAbility associations using the actual,
// permenant id, which means that the bird needs to be saved first.
                TempData["message"] = $"{bird.Name} has been saved.";
                return RedirectToAction("Index");
            }else{
                TempData["message"] = $"{bird.Name} has NOT been saved.";
				if(birdAbilityIds2!=null){
					List<Ability> ab = new List<Ability>();
					for(int i = 0; i < birdAbilityIds2.Length; i++){
						foreach(Ability a in abilityRepo.Abilities){
							if(birdAbilityIds2[i] == a.AbilityId) {
								ab.Add(a);
							}
						}
					}
					bird.Abilities2 = ab;
				}
                return View(bird);
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult EditAbility(Ability Ability) {
            if(ModelState.IsValid) {
                abilityRepo.SaveAbility(Ability);
                TempData["message"] = $"{Ability.Name} has been saved.";
                return RedirectToAction("Index");
            }else{
                return View(Ability);
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult DeleteBird(int BirdId) {
            Bird pr = birdRepo.Birds.FirstOrDefault(p=> p.BirdId == BirdId);
            if(pr!=null){
                TempData["message"] = $"{pr.Name} has been deleted.";
                birdRepo.DeleteBird(pr);
			
            } else {
                TempData["message"] = "Something seems to have gone very wrong!!!";
            }
            return RedirectToAction("Index");
        }

		[Authorize]
        [HttpPost]
        public IActionResult DeleteAbility(int AbilityId) {
            Ability a = abilityRepo.Abilities.FirstOrDefault(p=> p.AbilityId == AbilityId);
            if(a!=null){
                TempData["message"] = $"{a.Name} has been deleted.";
                abilityRepo.DeleteAbility(a);
			
            } else {
                TempData["message"] = "Something seems to have gone very wrong!!!";
            }
            return RedirectToAction("Index");
        }

        [Authorize]
        public Bird ApplyAbilitiesToBird(Bird bird) {
            List<Ability> abilities = new List<Ability>();
            foreach(BirdAbility ba in birdAbilityRepo.BirdAbilities) {
                if(ba.BirdId == bird.BirdId) {           
                    abilities.Add(abilityRepo.Abilities.FirstOrDefault(
                        a=>a.AbilityId == ba.AbilityId)    
                    );
                }
            }
            bird.Abilities2 = abilities;
            return bird;
        }

		private int[] parseStringToInts(string s){
			List<int> a = new List<int>();
			string subS="";
			for(int i = 0; i < s.Length; i++){
				if(s[i] != ',' && s[i] != ' '){
					subS += s[i];
				}else if (subS != ""){
					a.Add(Int32.Parse(subS));
					subS = "";
				}
				if( i == s.Length-1 && subS!=""){
					a.Add(Int32.Parse(subS));
				}
			}
			return a.ToArray();
		}
    }
}