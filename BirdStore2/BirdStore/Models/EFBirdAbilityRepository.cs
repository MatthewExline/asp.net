﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models {
    public class EFBirdAbilityRepository : IBirdAbilityRepository {
        public ApplicationDbContext context;
        public IEnumerable<Bird>Birds =>context.Birds;
        public IEnumerable<Ability>Abilities=>context.Abilities;
        public IEnumerable<BirdAbility>BirdAbilities=>context.BirdAbilities;

        public EFBirdAbilityRepository(ApplicationDbContext context) {
            this.context = context;
        }
		public void AddBirdAbility(int birdId, int abilityId){
			BirdAbility ba = new BirdAbility{
			   Bird=context.Birds.FirstOrDefault(p=>p.BirdId==birdId),
			   BirdId=birdId,
			   Ability=context.Abilities.FirstOrDefault(a=>a.AbilityId==abilityId),
			   AbilityId=abilityId
			};
			context.BirdAbilities.AddRange(ba);
			context.SaveChanges();
		}
		public void Delete(List<BirdAbility> ba){
			context.BirdAbilities.RemoveRange(ba);
			context.SaveChanges();
		}
    }
}
