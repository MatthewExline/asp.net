﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models {
    public class BirdAbility {
        public int BirdId{get; set;}
        public Bird Bird {get; set;}
        public int AbilityId{get; set;}
        public Ability Ability{get; set;}
    }
}
