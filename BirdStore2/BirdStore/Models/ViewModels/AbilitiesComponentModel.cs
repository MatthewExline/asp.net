﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models.ViewModels
{
    public class AbilitiesComponentModel {
		public List<Ability> AllAbilities {get; set;}
		public List<int> SelectedAbilities {get; set;}
    }
}
