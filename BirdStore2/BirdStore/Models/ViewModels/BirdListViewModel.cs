﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models.ViewModels
{
    //this is so that we can send it page number and data
    public class BirdListViewModel
    {
        public IEnumerable<Bird> Birds { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string CurrentCategory { get; set; }
    }
}
