﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models {
    public class LoginModel {
        [Required(ErrorMessage ="Your username seems to be incorrect.")]
        public string Name {get; set;}

        [Required]
        [UIHint("password")]
        public string Password { get; set; }

        public string ReturnUrl { get; set; } = "/Admin/Index/";
    }
}
