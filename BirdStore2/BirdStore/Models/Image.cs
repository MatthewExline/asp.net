﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models {
    public class Image {
        public int ImageId{get; set;}
		public string Name{get; set;}
        public byte[] Picture {get; set;}
		public string ContentType {get; set;}

		//association -
        public int BirdId {get; set;}
        public Bird Bird{get; set;}
    }
}
