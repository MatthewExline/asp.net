﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models {
    public class Cart {


        //Association
        private List<CartLine> lineCollection = new List<CartLine>();
        //below line is to make the lineCollection accessible since it is private. Hides what is going on underneath hood.
        public IEnumerable<CartLine> Lines => lineCollection;

        
        //virtual means that you can override it in a subclass. Appartently in C# you cannot override unless you have virtual keyword...
        public virtual void AddItem(Bird prod, int qty)
        {
            //before we add something, let's make sure it is not already in there.
            CartLine line = lineCollection
                .Where(l => l.Bird.BirdId == prod.BirdId) //have to go into the line, which has Birds
                .FirstOrDefault(); //this is in case Where clause returns more than one thing. In our case, this is just good practice since we are asking for Id's.

            
            if (null == line)
            {
                //if the item is not in the cart, put it in
                lineCollection.Add(
                    new CartLine()
                    {
                        Bird = prod,
                        Quantity = qty

                    });


            }
            else //if it is already in cart, just increment the Quantity
            {
                line.Quantity += qty;
            }

        }


        public virtual void Clear() => lineCollection.Clear();


        public virtual void RemoveLine(Bird prod)
        {
            lineCollection.RemoveAll(l => l.Bird.BirdId == prod.BirdId);

        }

        public virtual decimal ComputeTotalValue()
        {
            return lineCollection.Sum(e => e.Bird.Price * e.Quantity);
        }


    }



    public class CartLine
    {

        public int CartLineId { get; set; }
        public Bird Bird { get; set; }
        public int Quantity { get; set; }

    }


}
