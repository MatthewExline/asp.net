﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models
{
    public interface IBirdRepository {
        IEnumerable<Bird> Birds { get; }
        void SaveBird(Bird Bird);
        Bird DeleteBird(Bird Bird);
    }    
}
