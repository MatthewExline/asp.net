﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models {
    public interface IAbilityRepository {        
        IEnumerable<Ability> Abilities { get; }
        void SaveAbility(Ability Abiltity);
        Ability DeleteAbility(Ability Ability);
    }
}
