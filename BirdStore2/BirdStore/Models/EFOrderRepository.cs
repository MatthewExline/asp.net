﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models
{
    public class EFOrderRepository : IOrderRepository {
        private ApplicationDbContext context;
        public EFOrderRepository(ApplicationDbContext con) {
            context = con;
        }
        public IEnumerable<Order> Orders => context.Orders
            .Include(o => o.Lines)//this is like a join. We're pulling the associations.
            .ThenInclude(l => l.Bird);
        public void SaveOrder(Order order) {
            //don't resave the associated Birds    :-)
            context.AttachRange(order.Lines.Select(l => l.Bird));
            if(order.OrderId == 0) { //orders that don't exist yet have an id of zero until they are assigned
                context.Orders.Add(order);                
            }
			context.SaveChanges();
        }
    }
/*
 * public IEnumerable<Author>Authors
 */
}
