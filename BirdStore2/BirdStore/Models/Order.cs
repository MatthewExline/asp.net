﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models {
    public class Order {
        //CartLines will be the junction table between Birds and Orders
        [BindNever]//out on the view, the view cannot bind to this
        public int OrderId {get; set;}
        [BindNever]
        public ICollection<CartLine>Lines{get; set;}

        [Required(ErrorMessage = "Please enter a name.")]
        public string Name {get; set;}//who placed the order
        [Required(ErrorMessage = "Please enter an address.")]
        public string Line1 {get; set;}
        public string Line2 {get; set;}
        public string Line3 {get; set;}
        [Required(ErrorMessage = "Please enter a City name.")]
        public string City {get; set;}
        [Required(ErrorMessage = "Please enter a State name.")]
        public string State {get; set;}
        [Required(ErrorMessage = "Please enter a zip code.")]
        public string Zip {get; set;}
        [Required(ErrorMessage = "Please enter a Country name.")]
        public string Country {get; set;}
        public Boolean GiftWrap {get; set;}

        [BindNever]//won't let asp view bind it???
        public bool Shipped {get; set; }

    }
}
