﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models
{
    public class EFAbilityRepository : IAbilityRepository {
        private ApplicationDbContext context;

        public EFAbilityRepository(ApplicationDbContext context) {
            this.context = context;
        }

        public IEnumerable<Ability> Abilities => context.Abilities;//this is how you get at Birds from DB.

        public void SaveAbility(Ability Ability) {
            if(Ability.AbilityId == 0) {
                context.Abilities.Add(Ability);
            }else{
                Ability efAbility = context.Abilities.FirstOrDefault(p => p.AbilityId == Ability.AbilityId);
                if(efAbility != null) {
                    efAbility.Name = Ability.Name;
                    efAbility.Birds = Ability.Birds;
                }
            }
            context.SaveChanges();
        }

        public Ability DeleteAbility(Ability Ability) {
            Ability pr = context.Abilities.FirstOrDefault(p=>p.AbilityId == Ability.AbilityId);
            if(pr!=null){
                context.Abilities.Remove(pr);
            }context.SaveChanges();
            return pr;
        }
    }
}
