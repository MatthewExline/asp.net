﻿/*
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models {
    public static class SeedData  {
        public static void EnsurePopulate(ApplicationDbContext context) {
			context.Images.UpdateRange(
				for(int i = 0; i < context.Images.Count(); i++){
					System.IO.MemoryStream m;
					System.Drawing. image = (IDisposable)Image.FromFile(context.Images.ElementAt(i).Name);
					context.Images.ElementAt(i).Picture = 
				}
			);
			context.SaveChanges();/*
            if (!context.Birds.Any())  {
                context.Birds.AddRange(
                    new Bird {
                        Name = "Emperor Penguin",
                        Description = "He's the new Bling-Bling-King!",
                        Category = "Penguin",
                        Price = 1275
                    },
                    new Bird {
                        Name = "Empress Penguin",
                        Description = "Bling-Bling-King with a feminine side!",
                        Category = "Penguin",
                        Price = 1375
                    },
                    new Bird {
                        Name = "HappyFeet",
                        Description = "My Feet? They're happy too!",
                        Category = "Penguin",
                        Price = 48.95m
                    },
                    new Bird {
                        Name = "Rooster",
                        Description = "A real man's alarm clock.",
                        Category = "Chicken",
                        Price = 19.50m
                    },
                    new Bird {
                        Name = "Hen",
                        Description = "Lays them golden eggs!",
                        Category = "Chicken",
                        Price = 34.95m
                    },
                    new Bird {
                        Name = "Armegeddon Penguin",
                        Description = "Rocket-launcher not included*",
                        Category = "Penguin",
                        Price = 79500
                    },
                    new Bird {
                        Name = "Devil-Parrot",
                        Description = "He'll sit on your shoulder and whisper bad advice into your ear.",
                        Category = "Parrot",
                        Price = 16
                    },
                    new Bird {
                        Name = "Angelic Parrot",
                        Description = "Always erring on the sides of caution and grace.",
                        Category = "Parrot",
                        Price = 29.95m
                    },
                    new Bird {
                        Name = "NSA Parrot",
                        Description = "Always listening to everything you say, and then repeating it when you least want him to.",
                        Category = "Parrot",
                        Price = 750
                    },
                    new Bird {
                        Name = "Well-Trained Crow",
                        Description = "Pecks HTML code for you.",
                        Category = "Crow",
                        Price = 1200
                    }
                );
                context.SaveChanges();
            }
            if (!context.Abilities.Any())  {
                context.Abilities.AddRange(
                    new Ability {
                        Name = "Flies!"
                    },
                    new Ability {
                        Name = "Wakes you up in the morning."
                    },
                    new Ability {
                        Name = "Tastes great!"
                    },
                    new Ability {
                        Name = "Adorable!",                        
                    },
                    new Ability {
                        Name = "Swims!"
                    },
                    new Ability {
                        Name = "Lays Eggs!"
                    },
                    new Ability {
                        Name = "Unleashes Chaos and Destruction!"
                    },
                    new Ability {
                        Name = "Gives Advice."
                    },
                    new Ability  {
                        Name = "Types HTML code."
                    },
                    new Ability {
                        Name = "Rules with an Iron Fist."
                    }
                );
                context.SaveChanges();
            }
            if(!context.Images.Any()){
                context.Images.AddRange(
                    new Image {
                        Path="~/Images/angelicParrot0.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==8),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==8).BirdId
                    }, new Image {
                        Path="~/Images/angelicParrot1.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==8),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==8).BirdId
                    }, new Image {
                        Path="~/Images/angelicParrot0.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==8),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==8).BirdId
                    },new Image {
                        Path="~/Images/ap0.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==6),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==6).BirdId
                    },new Image {
                        Path="~/Images/ap1.png",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==6),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==6).BirdId
                    },new Image {
                        Path="~/Images/ap2.JPG",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==6),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==6).BirdId
                    },new Image {
                        Path="~/Images/ap3.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==6),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==6).BirdId
                    },new Image {
                        Path="~/Images/ap4.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==6),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==6).BirdId
                    },new Image {
                        Path="~/Images/ap5.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==6),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==6).BirdId
                    },new Image {
                        Path="~/Images/ap6.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==6),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==6).BirdId
                    },new Image {
                        Path="~/Images/devilParrot0.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==7),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==7).BirdId
                    },new Image {
                        Path="~/Images/ep0.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==1),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==1).BirdId
                    },new Image {
                        Path="~/Images/ep1.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==1),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==1).BirdId
                    },new Image {
                        Path="~/Images/ep2.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==1),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==1).BirdId
                    },new Image {
                        Path="~/Images/ep3.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==1),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==1).BirdId
                    },new Image {
                        Path="~/Images/ep3.png",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==1),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==1).BirdId
                    },new Image {
                        Path="~/Images/ep4.png",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==1),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==1).BirdId
                    },new Image {
                        Path="~/Images/ep5.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==1),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==1).BirdId
                    },new Image {
                        Path="~/Images/ep6.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==1),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==1).BirdId
                    },new Image {
                        Path="~/Images/esp0.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==2),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==2).BirdId
                    },new Image {
                        Path="~/Images/h0.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==5),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==5).BirdId
                    },new Image {
                        Path="~/Images/h1.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==5),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==5).BirdId
                    },new Image {
                        Path="~/Images/h2.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==5),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==5).BirdId
                    },new Image {
                        Path="~/Images/h3.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==5),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==5).BirdId
                    },new Image {
                        Path="~/Images/hf0.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==3),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==3).BirdId
                    },new Image {
                        Path="~/Images/hf1.png",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==3),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==3).BirdId
                    },new Image {
                        Path="~/Images/hf2.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==3),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==3).BirdId
                    },new Image {
                        Path="~/Images/html0.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==10),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==10).BirdId
                    },new Image {
                        Path="~/Images/NSAParrot0.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==9),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==9).BirdId
                    },new Image {
                        Path="~/Images/NSAParrot1.png",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==9),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==9).BirdId
                    },new Image {
                        Path="~/Images/r0.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==4),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==4).BirdId
                    },new Image {
                        Path="~/Images/r1.png",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==4),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==4).BirdId
                    },new Image {
                        Path="~/Images/r2.jpg",
                        Bird = context.Birds.FirstOrDefault(b=>b.BirdId==4),
                        BirdId = context.Birds.FirstOrDefault(b=>b.BirdId==4).BirdId
                    }                    
                );
                context.SaveChanges();
            }
            if (!context.BirdAbilities.Any())  {
                context.BirdAbilities.AddRange(                    
                    new BirdAbility {                        
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Emperor Penguin"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Emperor Penguin").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Swims!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Swims!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Emperor Penguin"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Emperor Penguin").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Rules with an Iron Fist."),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Rules with an Iron Fist.").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Empress Penguin"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Empress Penguin").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Lays Eggs!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Lays Eggs!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Empress Penguin"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Empress Penguin").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Swims!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Swims!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Empress Penguin"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Empress Penguin").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Rules with an Iron Fist."),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Rules with an Iron Fist.").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="HappyFeet"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="HappyFeet").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Swims!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Swims!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="HappyFeet"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="HappyFeet").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Adorable!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Adorable!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Armegeddon Penguin"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Armegeddon Penguin").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Swims!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Swims!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Armegeddon Penguin"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Armegeddon Penguin").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Unleashes Chaos and Destruction!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Unleashes Chaos and Destruction!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Rooster"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Rooster").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Wakes you up in the morning."),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Wakes you up in the morning.").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Rooster"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Rooster").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Tastes great!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Tastes great!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Hen"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Hen").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Lays Eggs!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Lays Eggs!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Hen"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Hen").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Tastes great!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Tastes great!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Devil-Parrot"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Devil-Parrot").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Flies!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Flies!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Angelic Parrot"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Angelic Parrot").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Flies!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Flies!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Devil-Parrot"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Devil-Parrot").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Gives Advice."),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Gives Advice.").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Angelic Parrot"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Angelic Parrot").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Gives Advice."),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Gives Advice.").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="NSA Parrot"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="NSA Parrot").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Flies!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Flies!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="NSA Parrot"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="NSA Parrot").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Unleashes Chaos and Destruction!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Unleashes Chaos and Destruction!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Well-Trained Crow"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Well-Trained Crow").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Flies!"),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Flies!").AbilityId,
                    },new BirdAbility {
                        Bird=context.Birds.FirstOrDefault(p=>p.Name=="Well-Trained Crow"),
                        BirdId=context.Birds.FirstOrDefault(p=>p.Name=="Well-Trained Crow").BirdId,
                        Ability=context.Abilities.FirstOrDefault(a=>a.Name=="Types HTML code."),
                        AbilityId=context.Abilities.FirstOrDefault(a=>a.Name=="Types HTML code.").AbilityId,
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
*/