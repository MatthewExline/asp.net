﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using BirdStore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Models {
    public class SessionCart : Cart {

        [JsonIgnore] //THIS JsonIgnore IS SUPER IMPORTANT!!!
        public ISession Session { get; set; }


        public static Cart GetCart(IServiceProvider services) //services passed from Startup w/ dependency injection
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?
                .HttpContext.Session;

            SessionCart cart = session?.GetJson<SessionCart>("Cart") ??
                new SessionCart();//if there is no SessionCart, make a new one.

            cart.Session = session;

            return cart; 

        }


        public override void AddItem(Bird prod, int qty)//we are overriding this from cart so that we can save cart to session.
        {

            base.AddItem(prod, qty);
            Session.SetJson("Cart", this);

        }


        public override void RemoveLine(Bird prod)
        {
            base.RemoveLine(prod);
            Session.SetJson("Cart", this);
        }


        public override void Clear()
        {
            base.Clear();
            Session.Remove("Cart");
        }

    }
}
