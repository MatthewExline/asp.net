﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BirdStore.Infrastructure
{
    //Extension methods in order to add functionality to some classes that are locked away from us.
    public static class UrlExtensions
    {
        //we are adding this so that when you go "back" from the ShoppingCart, it will take you back to specifically where you were
        //such as a list of a specific category instead of just the general list (which is all the .Path does in the View)
        public static string PathAndQuery(this HttpRequest request)
        {

            //the $"{}" stuff below is just some fancy string formatting
            return request.QueryString.HasValue ? $"{request.Path.ToString()}{request.QueryString}" : request.Path.ToString();
        }





    }
}
