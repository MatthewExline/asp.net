﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BirdStore.Models;
using BirdStore.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace BirdStore.Components {
    public class AllAbilitiesDropDownComponent : ViewComponent {
		private IAbilityRepository AbilityRepo;
		private AbilitiesComponentModel acm;
        public AllAbilitiesDropDownComponent(IAbilityRepository a) {
            this.AbilityRepo = a;
        }

		public IViewComponentResult Invoke(List<Ability> list) {
			acm = new AbilitiesComponentModel();
			List<int> list2 = new List<int>();
			if(list!=null)
				foreach(Ability a in list){
					list2.Add(a.AbilityId);					
				}
			acm.SelectedAbilities = list2;
			acm.AllAbilities = AbilityRepo.Abilities
                .Distinct().ToList();
            return View(acm);
        }
    }
}