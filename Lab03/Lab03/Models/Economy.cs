﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models{
    public class Economy {
        public double GDP {get; set;}
        public double CleanWater {get; set;}
        public double SubsistanceFarming {get; set;}
        public double LuxaryFoods {get; set;}
        public double CheapLabor {get; set;}
        public double SkilledLabor {get; set;}
        public double SpecializedLabor {get; set;}
        public double Tourism {get; set;}
        public double Population {get; set;}
        public double SpecializedShippingAbilities {get; set;}
        public double EssentialManufacturing {get; set;}
        public double ExtravagantManufacturing {get; set;}
        public double SpecializedManufacturing {get; set;}
        public double NaturalResources {get; set;}
        public double LargeScaleMilitaryOffense {get; set;}
        public double NuclearOffense {get; set;}
        public double ClandestineMilitaryOffense {get; set;}
        public double LargeScaleMilitaryDefense {get; set;}
        public double NuclearDefense { get {
            return (NuclearOffense + ClandestineMilitaryDefense +
                .5*ClandestineMilitaryOffense)/2.5;
            }
        }
        public double ClandestineMilitaryDefense {get; set;}

        public Economy(){

        }        
    }
}
