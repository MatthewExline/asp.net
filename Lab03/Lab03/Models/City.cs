﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models{
    public class City{
        public int cIdNum{get; set;}
        public string Name{get; set;}
        public double GDP{get; set;}
        public double[] Coordinates{get; set;}//left, right, top, bottom
        public bool Within(double x, double y){
            if(Coordinates[1] <= x && Coordinates[2] >= x
            && Coordinates[3] <= y && Coordinates[4] >= y) 
                return true;
            else return false;
        }        
    }
}
