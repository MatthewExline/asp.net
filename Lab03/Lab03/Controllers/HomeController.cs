﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab03.Models;
using Microsoft.AspNetCore.Mvc;

namespace Lab03.Controllers{
    public class HomeController : Controller{
        public IActionResult Index(){
            City[] cities = new City[]{//left, right, top, bottom
                new City{cIdNum = 1, Name="N. Dakota", 
                    Coordinates= new double[4]{.169, .186, .803, .782}, GDP=20},
                new City{cIdNum = 2, Name="La Crosse", 
                    Coordinates=new double[4]{.189, .206, .773, .750}, GDP = 10},
                new City{cIdNum = 3, Name="Utah",
                    Coordinates=new double[4]{.113, .147, .774, .737}, GDP = 30},
                new City{cIdNum = 4, Name="East Coast", 
                    Coordinates=new double[4]{.229, .275, .763, .707}, GDP = 45},
                new City{cIdNum = 5, Name="Cuba", 
                    Coordinates=new double[4]{.229, .249, .600, .579}, GDP = 20},
                new City{cIdNum = 6, Name="Mexico", 
                    Coordinates=new double[4]{.167, .212, .498, .442}, GDP = 30},
                new City{cIdNum = 7, Name="Russia", 
                    Coordinates=new double[4]{.581, .638, .818, .738}, GDP = 400},
                new City{cIdNum = 8, Name="Mongolia", 
                    Coordinates=new double[4]{.653, .676, .768, .741}, GDP = 200},
                new City{cIdNum = 9, Name="China", 
                    Coordinates=new double[4]{.612, .653, .680, .642}, GDP=700},
                new City{cIdNum = 10, Name="Middle East", 
                    Coordinates=new double[4]{.550, .572, .657, .634}, GDP=400},
                new City{cIdNum = 11, Name="Germany", 
                    Coordinates=new double[4]{.425, .461, .666, .627}, GDP=400},
                new City{cIdNum = 12, Name="Spain", 
                    Coordinates=new double[4]{.417, .436, .617, .592}, GDP = 300},
                new City{cIdNum = 13, Name="Egypt", 
                    Coordinates=new double[4]{.480, .500, .569, .546}, GDP = 300},
                new City{cIdNum = 14, Name="Japan", 
                    Coordinates=new double[4]{.776, .886, .923, .755}, GDP = 700},
                new City{cIdNum = 15, Name="Philippines", 
                    Coordinates=new double[4]{.754, .808, .541, .474}, GDP = 100},
                new City{cIdNum = 16, Name="Indonesia", 
                    Coordinates=new double[4]{.702, .754, .463, .400}, GDP = 200},
                new City{cIdNum = 17, Name="Australia", 
                    Coordinates=new double[4]{.662, .814, .340, .202}, GDP=400 },
                new City{cIdNum = 18, Name="Rio Grande", 
                    Coordinates=new double[4]{.240, .286, .170, .111}, GDP=20},
                new City{cIdNum = 19, Name="Primavera", 
                    Coordinates=new double[4]{.209, .229, .183, .153}, GDP = 10},
                new City{cIdNum = 20, Name="Greenland ", 
                    Coordinates=new double[4]{.285, .334, .965, .902}, GDP = 400}
            };
            double totalGDP = 0;
            double southernGDP=0;
            double northernGDP=0;
            foreach(City c in cities){
                totalGDP += c.GDP;
                if(c.Coordinates[3]<.64) southernGDP+= c.GDP;
                else northernGDP += c.GDP;
            }
            ViewBag.WorldMarketValue = totalGDP;
            ViewBag.SouthernGDP = southernGDP;
            ViewBag.NorthernGDP = northernGDP;
            return View(cities);
        }
    }
}