﻿var cities;
var currentCity;
var highlightCities = false;
var cityAreas = null;
document.addEventListener(onload, function () {
    var cityPanel = document.getElementById("cityPanel");
});

document.addEventListener("click", function (e) {
    if (document.activeElement.type != "button" && document.activeElement.type!="option" && document.activeElement.type!="select-one" && document.activeElement.type!="select") {
        //var x = (e.pageX + document.body.offsetLeft) / document.body.scrollWidth;
        var x = e.pageX / document.body.scrollWidth;        
        //var y = (e.pageY + document.body.offsetTop) / document.body.scrollHeight;
        var y = (document.body.scrollHeight - e.pageY) / document.body.scrollHeight;//scrollHeight, offsetHeight, clientHeight
        //types of x's and y's for mouseclick events: client, offset, page, screen, regular
        var clickedOnACity = false; 
        cities.forEach(function (c) {
            if (within(x, y, c.coordinates)) {
                currentCity = c;
                setPanel();
                clickedOnACity = true;
            }      
        });
        if (!clickedOnACity) alert("x: " + x + "   y: " + y);
    }
});

function toggleCityHighlights() {
    if (!highlightCities) {//if we're turning the highlights on...
        document.getElementById("cityHighlighter").innerText = "Hide Highlights...";
        if (cityAreas == null) {
            //if the cityAreas don't exist yet (aka it's null) then we build it
            cities.forEach(function (c) {
                var newDiv = document.createElement("div");
                newDiv.setAttribute("id", "cityArea" + c.cIdNum);
                newDiv.setAttribute("class", "cityArea");
                newDiv.style.position = "absolute";
                newDiv.style.backgroundColor = "red";
                newDiv.style.zIndex = 12;
                newDiv.style.left = (c.coordinates[0] * 100) + "%";
                //the line below this comment took about 7 hours to write and I don't think I'm exaggerating.
                newDiv.style.top = document.body.scrollHeight - (c.coordinates[2] * document.body.scrollHeight) + "px";
                newDiv.style.width = ((c.coordinates[1] - c.coordinates[0]) * 100) + "%";
                newDiv.style.height = ((c.coordinates[2] * document.body.scrollHeight - c.coordinates[3] * document.body.scrollHeight)) + "px";
                document.body.appendChild(newDiv);
                cityAreas = document.querySelectorAll(".cityArea");
            });
        } else {//if it was already built then we just have to set it to visible.
            for (var i = 0; i < cityAreas.length; i++) {
                cityAreas[i].style.left = (cities[i].coordinates[0] * 100) + "%";                
                cityAreas[i].style.top = document.body.scrollHeight - (cities[i].coordinates[2] * document.body.scrollHeight) + "px";
                cityAreas[i].style.width = ((cities[i].coordinates[1] - cities[i].coordinates[0]) * 100) + "%";
                cityAreas[i].style.height = ((cities[i].coordinates[2] * document.body.scrollHeight - cities[i].coordinates[3] * document.body.scrollHeight)) + "px";
                cityAreas[i].style.display = "flex";
            }
        }
    } else {
        document.getElementById("cityHighlighter").innerText = "Highlight City Areas";
        for (var i = 0; i < cityAreas.length; i++) {
            cityAreas[i].style.display = "none";
        }
    }  
    if (!highlightCities) highlightCities = true;
    else highlightCities = false;
}

function within(x, y, Coordinates){
    if (Coordinates[0] <= x && Coordinates[1] >= x
        && Coordinates[2] >= y && Coordinates[3] <= y)
        return true;
    else {        
        return false;
    }
}

function setPanel(cityID) {
    cityPanel.style.display = "block";
    cityPanel.innerHTML = currentCity.name;
}
function closePanel() {
    cityPanel.style.display = "none";    
}