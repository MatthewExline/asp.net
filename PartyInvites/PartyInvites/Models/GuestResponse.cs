﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PartyInvites.Models {
    public class GuestResponse {
        
        //public string Name { get; set;  }
        [Required(ErrorMessage = "Please enter your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter your email")]
        //[RegularExpression(".+\\@.+\\..+")] //this will work on its own also. But even better if we do:
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Enter a valid email.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your phone number")]
        [RegularExpression("^(1?(-?\\d{3})-?)?(\\d{3})(-?\\d{4})$", ErrorMessage = "Enter a valid phone number.")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Please indicate whether or not you will attend.")]
        public bool? WillAttend { get; set; } //the questionmark means it can be true, false, or null
        //primitives need the ? if you want to make them nullable.  Objects are ALWAYS nullable.
    }
}
