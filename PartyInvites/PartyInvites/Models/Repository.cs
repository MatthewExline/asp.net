﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PartyInvites.Models {
    public static class Repository {
        private static List<GuestResponse> responses = 
            new List<GuestResponse>();

        //Ienumerable ..- It's an interface and a list type. You can iterate through it.
        //You can use a foreach() loop and process it.  It keeps track of pointers.?
        public static IEnumerable<GuestResponse> Responses{
            get{
                return responses;
            }
        }

        public static void AddResponse(GuestResponse response){
            responses.Add(response);
        }

    }
}
