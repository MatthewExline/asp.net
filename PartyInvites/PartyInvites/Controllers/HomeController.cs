﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PartyInvites.Models;

namespace PartyInvites.Controllers
{
    public class HomeController : Controller //  The colon means 'extends'.
    {
        //every method within a controller is called an action.
        public IActionResult Index() {
            int hour = DateTime.Now.Hour;
            string greeting = hour < 12 ? "Good Morning" : "Good Afternoon";
            greeting += ", the current time is " + DateTime.Now.ToShortTimeString();
            ViewBag.Greeting = greeting;
            return View();
        }
        //when a class is prefaced with "I" it implies Interface
        [HttpGet]
        public ViewResult RsvpForm() {
            return View();
        }
        //Models folder always contains database stuff and business logic
        [HttpPost]
        public ViewResult RsvpForm(GuestResponse resp) {
            if(ModelState.IsValid){
                Repository.AddResponse(resp);
                return View("Thanks", resp);
            }else{
                return View(resp);//giving the view back to the form, and loading it up back as before.
            }
        }
        public IActionResult ListResponses(){
            //all view results are IAction Results. We could write this as:
            //public ViewResult ListResponses(){  }   .  If you're returning another view that's an action,
            //then you need to return an IActionResult.
            return View(Repository.Responses.Where(r => r.WillAttend == true));
        }
    }
}