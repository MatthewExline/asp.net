﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace LanguageFeatures
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services){
            services.AddMvc();//this is the line of code that we add (eluded to below) about adding MVC
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env){
            app.UseDeveloperExceptionPage();//don't use this in production, this is for building. This can show
//your database structure and you can get hacked.
            app.UseMvcWithDefaultRoute();
            //we need to add mvc also, but this will get us up and running. We don't actually need to write our
            //routes (home, index, etc) like we did before.
        }
    }
}
