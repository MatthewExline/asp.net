﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LanguageFeatures.Models;
using Microsoft.AspNetCore.Mvc;

namespace LanguageFeatures.Controllers {
    public class HomeController : Controller {
        /*
        public ViewResult Index()//a ViewResult is an iActionResult
        {
            return View(new string[] { "C#", "Language", "Features"});
        }
        */
        //public ViewResult Index(){
        //    List<string> results = new List<string>();
        //    foreach (Product p in Product.GetProducts()) {
        //        /*
        //        if(p != null){
        //            string name = p.Name;  decimal? price = p.Price;
        //            results.Add(string.Format("Name: [0], Price: {1}", name, price));
        //        }*/
        //        string name = p?.Name ?? "<No Name>";//?? means coalesse, or nullable coalessing operator
        //        //string name = p?.Name;
        //        decimal? price = p?.Price ?? 0;
        //        //decimal? price = p?.Price;
        //        /*
        //        results.Add(
        //            string.Format(
        //                "Name: {0}, Price: {1}, Related: {2}",
        //                name, price, p?.Related?.Name)
        //        );
        //        */
        //        results.Add($"Name: {name}, Price: {price:C2}, Related: {p?.Related?.Name ?? "<No relation>"}");
        //    }return View(results);//results;
        //} Ctrl + K + C is to mass-comment.
        
/*
        public ViewResult Index(){
            Dictionary<string, Product> products =
            new Dictionary<string, Product>(){//so a Dictionary is hashed. Its an indexed key-value pair. Very useful and fast. 
                //{ "Kayak", new Product(){Name="Kayak", Price = 275M} }
                //normally you load a dictionary from a database or a file or something.

                ["Kayak"] = new Product{Name = "Kayak", Price = 275M},
                ["Lifejacket"] = new Product {Name = "Lifejacket", Price = 48.95M }
                
            };
            //Product p = products["Kayak"];
            return View(products.Keys);
        }
*/
        
        /*Don't do this:
            ArrayList names = new ArrayList();
            names.Add
        */
        //Do this instead:
        public ViewResult Index2(){
            List<string> names = new List<string>();
            names.Add("Fred");
            names.Add("Ben");

            return View();
            
        }

        //public ViewResult Index(){
        //    ShoppingCart cart = new ShoppingCart {
        //        Products = Product.GetProducts()
        //    };
        //    Product[] prodArr = { 
        //        new Product{Name = "Canoe", Price=2000},
        //        new Product{Name = "Paddle", Price = 15 }
        //    };

        //    decimal totalPrice = cart.TotalPrices();
        //    return View("Index", 
        //        new string[]{
        //            $"Total: {totalPrice}",
        //            $"New Total: {prodArr.TotalPrices()}"}
        //        );
        //}


        
        //public ViewRestult Index(){
        //    ShoppingCart cart = new ShoppingCart{ Products = Product.GetProducts()};
        //    //decimal priceTotal = cart        
        //}
        //IEnumerable<Product> fProducts = cart.Filter(prop=> prop.);
        //public static IEnumerable<Product> Filter(
        //    this IEnumerable<Product> products, Func<Product, bool> selector){
        //        foreach(Product p in products){
        //            if(selector(p)) yield return p;
        //        }
        //}
        ShoppingCart cart = new ShoppingCart { Products = Product.GetProducts() };
        //public ViewResult Index(){
            
        //    ShoppingCart cart = new ShoppingCart {
        //        Products = Product.GetProducts()
        //    };
        //    Product[] prodArr = { 
        //        new Product{Name = "Canoe", Price=2000},
        //        new Product{Name = "Paddle", Price = 15 }
        //    };
        //    decimal nameTotal3 = cart.Filter(p=>p?.Name?[0]=='K').TotalPrices();

        //    decimal expensiveTotal = cart.FilterByPrice(20).TotalPrices();
        //    //decimal expensiveTotal2 = cart.Filter(FilterByPrice).TotalPrices();
        //    decimal nameTotal = cart.Filter(nameFilter).TotalPrices();
        //    decimal nameTotal2 = cart.FilterByName('K').TotalPrices();
        //    return View("Index", 
        //        new string[]{
        //            $"ExpensiveTotal: {expensiveTotal}",
        //            $"NameTotal: {nameTotal}"           
        //        }
        //    );
            
        //}
        //Func<Product, bool> nameFilter = delegate(Product p){
        //    return p?.Name?[0]=='S';
        //};
        
        public ViewResult Index(){
            return View(Product.GetProducts().Select(p=>p?.Name));//hey, grab the names out of that set
        }
    }
}