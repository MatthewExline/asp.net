﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LanguageFeatures.Models {
//extension methods...  sometimes you're given a class that you can't touch / modify.
    public sealed class ShoppingCart : IEnumerable<Product> {
        public IEnumerable<Product> Products { get; set; }

        public IEnumerator<Product> GetEnumerator() {
            return Products.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
//angular is google's javascript framework


        //public decimal CartTotal { get; set; }


        /* Do all of this in the getter instead...
        public decimal GetCartTotal(){
            foreach (Product p in Products){
                CartTotal = 0;
                CartTotal += p?.Price ?? 0;
            }
            return CartTotal;
        }*/
    }
}
