﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LanguageFeatures.Models
{
    public class Product {
        public string Name { get; set; }
        //these are public, but when you make these, they make private variables in the background.
        public decimal? Price { get; set; }
        public Product Related;
        public string Catetory {get; set; } = "Watersports";
        public bool InStock {get; } = true; //readonly, no setter.

        private string category;
        public string Category { 
            get{
                return category;
            }
            set{
                if(value == null)category = "";
                else category = value;
            }
        }
        //private string category;

        public static Product[] GetProducts(){

            //This is called an object initializer.  If you don't use it, you have to use . all over the place.
            Product kayak = new Product //putting in () makes it call the default constructor and it's optional
            {
                Name = "Kayak", Price = 275M 
            };

            Product lifejacket = new Product {
               Name = "Lifejacket", Price = 48.95M  
            };

            kayak.Related = lifejacket;

            return new Product[]{
                kayak, lifejacket, null
            };
        }
        public Product(bool stock = true){
            InStock = stock;
        }        
    }
}
