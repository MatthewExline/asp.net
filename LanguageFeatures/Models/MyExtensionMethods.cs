﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LanguageFeatures.Models
{
    public static class MyExtensionMethods {
        //public static decimal TotalPrices(this ShoppingCart cartParam){ //basically this is saying that this is an extension method
        //    decimal total = 0;
        //    foreach(Product p in cartParam.Products){
        //        //total += p.Price.Value; this works... but also...
        //        total += p?.Price ?? 0;
        //    }
        //    return total;
        //}
        public static decimal TotalPrices(this IEnumerable<Product> products){
            decimal total = 0;
            foreach(Product p in products){
                total += p?.Price ?? 0;
            }
            return total;
        }

        public static IEnumerable<Product> FilterByPrice(this IEnumerable<Product> products, decimal minPrice){
            List<Product>filProducts = new List<Product>();
            foreach(Product p in products){
                if(p?.Price > minPrice){
                    //filProducts.Add(p);
                    yield return p;//an optional way to do it...
                }
            }//return filProducts;
        }
        public static IEnumerable<Product> FilterByName(this IEnumerable<Product> products, char firstChar){
            List<Product>filProducts = new List<Product>();
            foreach(Product p in products){
                if(p?.Name?[0]==firstChar){
                    //filProducts.Add(p);
                    yield return p;//an optional way to do it...
                }
            }//return filProducts;
        }
        //public static IEnumerable<Product> Filter(this IEnumerable<Product> products, Func<Product, bool> selector){
        //    foreach(Product p in products){
        //        if(selector(p)) {  yield p;  }
        //    }
        //}
    }
}
